import urllib.parse as url_parse
from typing import Union

from aiohttp import web

# To store external URL configuration in "app".
CONST_PIECUT_EXTERNAL_URL_KEY = 'piecut_external_url'

parsed_url = Union[url_parse.ParseResult, url_parse.ParseResultBytes]


def external_url_set(app: web.Application, url: parsed_url):
    '''
    Sets an external URL for a web application in its state.
    '''
    app[CONST_PIECUT_EXTERNAL_URL_KEY] = url


def external_url_get(request: web.Request) -> parsed_url:
    '''
    Extracts an external URL from a web application state.
    '''
    return request.app[CONST_PIECUT_EXTERNAL_URL_KEY]
