{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "piesec.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "piesec.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "piesec.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common annotations
*/}}
{{- define "piesec.annotations" -}}
{{- with (coalesce .Values.global.extraAnnotations .Values.extraAnnotations) -}}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "piesec.labels" -}}
helm.sh/chart: {{ include "piesec.chart" . }}
{{ include "piesec.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "piesec.selectorLabels" -}}
app.kubernetes.io/name: {{ include "piesec.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- with (coalesce .Values.global.extraLabels .Values.extraLabels) }}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Name of the service account to use
*/}}
{{- define "piesec.serviceAccountName" -}}
{{- default (include "piesec.fullname" .) .Values.serviceAccount.name }}
{{- end }}

{{/*
Create admission service fullname with namespace as domain.
*/}}
{{- define "piesec.service.fullname" -}}
{{- default ( printf "%s.%s.svc" (include "piesec.fullname" .) .Release.Namespace ) -}}
{{- end -}}

{{/*
Generate TLS for admission-controller webhook
*/}}
{{- define "piesec.gen-certs" -}}
{{- $expiration := (.Values.tls.expiration | int) -}}
{{- $ca :=  genCA ( printf "%s-ca" .Release.Name ) $expiration -}}
{{- $altNames := list ( include "piesec.service.fullname" . ) -}}
{{- $cert := genSignedCert ( include "piesec.fullname" . ) nil $altNames $expiration $ca -}}
caCert: {{ b64enc $ca.Cert }}
clientCert: {{ b64enc $cert.Cert }}
clientKey: {{ b64enc $cert.Key }}
{{- end -}}

{{/*
Construct the image to use
*/}}
{{- define "piesec.image" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.image.repository $tag -}}
{{- end -}}
{{- end -}}

{{/*
PieSec instance name
*/}}
{{- define "piesec.instance.name" -}}
{{- default .Release.Name .Values.instance.name }}
{{- end -}}
