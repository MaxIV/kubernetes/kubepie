from typing import Optional

from aiohttp import web

# Key to store current namespace for PieCut.
CONST_PIECUT_INSTANCE_NAME_KEY = "piecut_instance_name"
CONST_PIECUT_WAIT_LOOP_FILE_KEY = "piecut_wait_loop_file"
CONST_PIECUT_PIEDATA_HOSTNAME_OVERRIDE_CLAIM_KEY = (
    "piecut_piedata_hostname_override_claim"
)


def instance_name_set(app: web.Application, instance: str):
    """Stores Application instance name in Application context."""
    app[CONST_PIECUT_INSTANCE_NAME_KEY] = instance


def instance_name_get(request: web.Request) -> str:
    """Retrieves Application instance name from Application context."""
    return request.app[CONST_PIECUT_INSTANCE_NAME_KEY]


def wait_loop_file_set(app: web.Application, wait_loop_file: str):
    """Stores Application wait loop file in Application context."""
    app[CONST_PIECUT_WAIT_LOOP_FILE_KEY] = wait_loop_file


def wait_loop_file_get(request: web.Request) -> str:
    """Retrieves Application wait loop file from Application context."""
    return request.app[CONST_PIECUT_WAIT_LOOP_FILE_KEY]


def hostname_override_claim_set(app: web.Application, claim_name: Optional[str]):
    """Stores Token claim name which defined a PieData hostnameOverride in Application context."""
    app[CONST_PIECUT_PIEDATA_HOSTNAME_OVERRIDE_CLAIM_KEY] = claim_name


def hostname_override_claim_get(request: web.Request) -> Optional[str]:
    """Retrieves Token claim name which defined a PieData hostnameOverride from Application context."""
    try:
        return request.app[CONST_PIECUT_PIEDATA_HOSTNAME_OVERRIDE_CLAIM_KEY]
    except KeyError:
        return None
