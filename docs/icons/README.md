# KubePie Graphics Profile

7 KubePie components are represented by 7 rainbow colors:

| Component | Main color | Tint color | Comment |
| --- | --- | --- | --- |
| PieSec | `#fe3621` | `#fe8679` | |
| PiePass | `#fea901` | `#fecb66` | MAX IV Orange |
| PieEat | `#ffd700` | `#ffef99` | |
| PieCut | `#82be00` | `#b4d866` | MAX IV Green |
| PieDeck | `#326de6` | `#84a7f0` | Kubernetes blue|
| PieData | `#0057b7` | `#669ad3` | |
| PieTrack | `#b25fcf` | `#d09fe2` | |

The `Ubuntu` bold font is used for captions.
