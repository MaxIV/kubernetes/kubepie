# PieData image

PieData image is based on [PiePass image](../../PiePass/image/) using the same Apache installation.

Additionally PieData enables WebDAV modules and includes `PieDir` for generating directory index and necessary static web assets.
