import asyncio
import logging
import sys

import kopf
from kubepie.pieeat import pieeat

if __name__ == '__main__':
    logger = logging.getLogger('KubePie.PieEat')

    if pieeat.PIEEAT_NAMESPACE is None:
        logger.fatal(
            'Environment variable PIEEAT_NAMESPACE is empty, must be set')
        sys.exit(1)

    logger.info(
        'Startup parameters: %s',
        {
            'check_interval_sec': pieeat.CHECK_INTERVAL_SEC,
            'initial_delay_sec': pieeat.INITIAL_DELAY_SEC,
            'failed_release_delete_after': pieeat.FAILED_RELEASES_DELETE_AFTER,
            'namespace': pieeat.PIEEAT_NAMESPACE,
            'failure_threshold_count': (
                pieeat.LOG_METRICS_FAILURE_COUNT_THRESHOLD),
            'liveness_port': pieeat.LIVENESS_PORT,
            'service_name_format': '{svc}:{port}'.format(
                svc=pieeat.PIEDATA_SERVICE_NAME_FORMAT,
                port=pieeat.PIEDATA_SERVICE_PORT),
        })

    asyncio.run(pieeat.init_kube_client())

    asyncio.run(
        kopf.operator(
            clusterwide=False,
            namespaces=(pieeat.PIEEAT_NAMESPACE,),
            liveness_endpoint='http://0.0.0.0:{}'.format(
                pieeat.LIVENESS_PORT)))
