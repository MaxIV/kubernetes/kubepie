import urllib.parse as url_parse
from typing import Optional, Union

from kubepie.piecut.lib.oidc_user_info import OIDCUserInfo


def compute_user_url(
        user_info: OIDCUserInfo,
        root_url: Union[url_parse.ParseResult,
                        url_parse.ParseResultBytes],
        original_url: Optional[str]) -> url_parse.ParseResult:
    if isinstance(root_url, url_parse.ParseResult):
        # Compute new URL path.
        new_path = url_parse.urljoin(
            root_url.path, url_parse.quote_plus(user_info.sub)) + '/'

        if original_url is not None:
            original_url = original_url.lstrip('/').removeprefix(user_info.sub)

            new_path = url_parse.urljoin(new_path, original_url)

        new_url = url_parse.ParseResult(
            root_url.scheme,
            root_url.netloc,
            new_path,
            root_url.params,
            root_url.query,
            root_url.fragment)
    elif isinstance(root_url, url_parse.ParseResultBytes):
        # Compute new URL path.
        new_path = url_parse.urljoin(
            root_url.path.decode('ascii'), url_parse.quote_plus(
                user_info.sub)) + '/'

        if original_url is not None:
            original_url = original_url.lstrip('/').removeprefix(user_info.sub)
            new_path = url_parse.urljoin(new_path, original_url)

        new_url = url_parse.ParseResult(
            root_url.scheme.decode('ascii'),
            root_url.netloc.decode('ascii'),
            new_path,
            root_url.params.decode('ascii'),
            root_url.query.decode('ascii'),
            root_url.fragment.decode('ascii')
        )
    else:
        raise ValueError('root_url has unsupported type: {type_}'.format(
            type_=type(root_url)))

    return new_url
