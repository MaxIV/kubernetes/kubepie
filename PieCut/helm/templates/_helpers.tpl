{{/*
Expand the name of the chart.
*/}}
{{- define "piecut.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.

The template differs from the default "helm create" by replacing:
{{- $name := default .Chart.Name .Values.nameOverride }}
with
{{- $name := default "piecut" .Values.nameOverride }}
so that if the template is passed to a dependency chart, the name refers to this chart,
in particular in the OIDC proxy "proxyBackends" field.
*/}}
{{- define "piecut.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default "piecut" .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "piecut.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common annotations
*/}}
{{- define "piecut.annotations" -}}
{{- with (coalesce .Values.global.extraAnnotations .Values.extraAnnotations) -}}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "piecut.labels" -}}
helm.sh/chart: {{ include "piecut.chart" . }}
{{ include "piecut.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "piecut.selectorLabels" -}}
app.kubernetes.io/name: {{ include "piecut.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- with (coalesce .Values.global.extraLabels .Values.extraLabels) }}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "piecut.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "piecut.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
PieSec instance name
*/}}
{{- define "piecut.instance.name" -}}
{{- default .Release.Name .Values.pieCutConfig.instanceName }}
{{- end -}}

{{/*
Construct the image to use
*/}}
{{- define "piecut.image" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.image.repository $tag -}}
{{- end -}}
{{- end -}}
