import logging
from .common import UserMap

#
# Plugin configuration provided as following JSON:
#     {
#         "map_key": "{preferred_username}"
#         "map": {
#             "nobody": {
#                 "uid":  65534,
#                 "gid":  65534,
#                 "sgids": [65534]
#             },
#             ....
#         }
#     }
#  NOTE! map_key includes the .format substitutions from claims

class StaticUserMap(UserMap):
    """ Implement static User Data mapping """
    def __init__(self, configs=[]) -> None:
        default_config = """{
            "map_key": "preferred_username",
            "map": {}
        }"""
        self.logger = logging.getLogger('KubePie.PieSec.Plugin.StaticMap')
        super().__init__(configs, defaults=default_config)

    def map_user(self, claims):
        # get map key from claims
        try:
            map_key_value = self.config['map_key'].format(**claims)
        except KeyError as e:
            self.logger.error('Failed to construct map key. The %s key is missing in the provided claims.', str(e))
            return False
        # lookup key in the static map
        if map_key_value not in self.config['map']:
            self.logger.error('User matching the claims %s is not found. No map key %s defined',
                              str(claims), map_key_value)
            return False
        map_data = self.config['map'][map_key_value]
        if 'uid' in map_data:
            self.data['uid'] = map_data['uid']
        if 'gid' in map_data: 
            self.data['gid'] = map_data['gid']
        if 'sgids' in map_data:
            self.data['sgids'] = map_data['sgids']
        self.logger.info('User mapped to: %s', str(map_data))
        return True

