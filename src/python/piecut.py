import asyncio
import logging
import os
import secrets
import sys
import urllib.parse as url_parse
from typing import Any, Dict, Tuple, Union, Iterable, Optional
import yaml

from aiohttp import web
from aiohttp.web_middlewares import normalize_path_middleware
from kubepie.common import kube_state
from kubepie.common.getenv import getenv, getenv_bool
from kubepie.common.piecrust import (
    PIEDATA_K8S_GROUP,
    PIEDATA_K8S_KIND_PLURAL,
    PIEDATA_K8S_VERSION,
)
from kubepie.piecut.web import handlers
from kubepie.piecut.web.external_url import external_url_set
from kubepie.piecut.web.config import (
    instance_name_set,
    wait_loop_file_set,
    hostname_override_claim_set,
)
from kubepie.piecut.web.middleware import (
    create_check_authentication_mw,
    save_original_path,
)
from kubepie.piecut.web.namespace import namespace_set
from kubepie.piecut.web.piedata import piedata_values_set
from kubepie.piecut.web.user_info import user_status_get_all_app, user_status_init
from kubernetes_asyncio import client, config, watch

__CONST_STATIC_FILES_PATH = "kubepie/piecut/static/"
__CONST_STATIC_FAVICON_PATH = os.path.join(__CONST_STATIC_FILES_PATH, "favicon.ico")
__CONST_STATIC_WAIT_LOOP_FILE = "piecut.html"


logger = logging.getLogger("KubePie.PieCut")


async def create_web_app(
    host: str,
    port: int,
    external_url: Union[url_parse.ParseResult, url_parse.ParseResultBytes],
    kube_namespace: str,
    instance_name: str,
    debug_token: str,
    extra_piedata_values: Dict[str, Any],
    static_files_path: str,
    favicon_path: str,
    wait_loop_file: str,
    kick_enabled: bool,
    kill_enabled: bool,
    log_level: str,
    piedata_token_claims_json: bool,
    hostname_claim: Optional[str],
) -> Tuple[web.Application, web.TCPSite]:
    logger.info("Initializing web API")
    web_app = web.Application(
        middlewares=[
            normalize_path_middleware(merge_slashes=True),
            create_check_authentication_mw(debug_token),
            save_original_path,
        ]
    )

    external_url_set(web_app, external_url)
    user_status_init(web_app)

    hostname_override_claim_set(web_app, hostname_claim)

    namespace_set(web_app, kube_namespace)
    piedata_values_set(web_app, extra_piedata_values)
    instance_name_set(web_app, instance_name)
    wait_loop_file_set(web_app, wait_loop_file)
    handlers.handlers_config_set(
        web_app,
        handlers.HandlersConfig(
            restart_enabled=kick_enabled,
            termination_enabled=kill_enabled,
            include_token_claims_to_piedata=piedata_token_claims_json,
        ),
    )

    # Configure aiohttp access log
    aio_log = logging.getLogger("aiohttp.access")

    aio_log.setLevel(log_level)
    log_handler_stdout = logging.StreamHandler(stream=sys.stdout)

    if log_level == "DEBUG":
        logging.getLogger().setLevel(logging.DEBUG)

    # Remove all default handlers as they cause duplicate messages.
    for h in aio_log.handlers:
        aio_log.removeHandler(h)
    aio_log.addHandler(log_handler_stdout)

    web_app.add_routes(
        [
            web.get(
                path="/kubepie/piecut/privileged/readiness",
                handler=handlers.is_ready,
                name="readiness",
            ),
            web.get(
                path="/kubepie/piecut/privileged/liveness",
                handler=handlers.is_alive,
                name="liveness",
            ),
            web.get(
                path="/kubepie/piecut/privileged/status/dump",
                handler=handlers.dump_statuses,
                name="dump_status",
            ),
            web.get(
                path="/kubepie/piecut/wait-piedata/{rand_string}",
                handler=handlers.spawn_piedata,
                name="wait-piedata",
            ),
            web.get(
                path="/kubepie/piecut/get-status",
                handler=handlers.get_status,
                name="get-piedata-status",
            ),
            # Static files.
            web.static(
                prefix="/kubepie/piecut/static",
                path=static_files_path,
                name="static-files",
            ),
            web.get(
                path="/favicon.ico",
                handler=handlers.init_favicon_handler(favicon_path),
                name="favicon",
            ),
            # Catch all routes to handle any unexpected requests
            # as a 'get me a KubePie PieData'.
            web.get(
                path="/",
                handler=handlers.spawn_piedata,
                name="root",
            ),
            web.get(
                path="/{path:.+}",
                handler=handlers.spawn_piedata,
                name="spawn-catchall",
            ),
        ]
    )

    if kill_enabled:
        web_app.add_routes(
            [
                web.post(
                    path="/kubepie/piecut/kill",
                    handler=handlers.kill_piedata,
                    name="kill-piedata-post",
                ),
                web.get(
                    path="/kubepie/piecut/kill",
                    handler=handlers.kill_piedata,
                    name="kill-piedata-get",
                ),
            ]
        )

    if kick_enabled:
        web_app.add_routes(
            [
                web.post(
                    path="/kubepie/piecut/kick",
                    handler=handlers.kick_piedata,
                    name="kick-piedata-post",
                ),
                web.get(
                    path="/kubepie/piecut/kick",
                    handler=handlers.kick_piedata,
                    name="kick-piedata-get",
                ),
            ]
        )

    runner = web.AppRunner(web_app)

    await runner.setup()

    site = web.TCPSite(runner, host=host, port=port)

    return web_app, site


async def run_kubernetes_piedata_watch(web_app: web.Application, namespace: str):
    logger.info("Loading Kubernetes configuration.")
    try:
        await config.load_kube_config()
    except config.ConfigException as e:
        logger.info("Cannot configure kube client via KUBECONFIG: %s", e)
        logger.info("Trying Kubernetes in-cluster config")

        config.load_incluster_config()

    logger.info("Starting watching PieData resources")

    # Might be not good to use the shared status here
    # instead of calling web_helpers.user_status_* functions.
    # ToDo: think about it later.
    status = user_status_get_all_app(web_app)

    watcher = watch.Watch()

    async with watcher.stream(
        client.CustomObjectsApi().list_namespaced_custom_object,
        group=PIEDATA_K8S_GROUP,
        version=PIEDATA_K8S_VERSION,
        plural=PIEDATA_K8S_KIND_PLURAL,
        namespace=namespace,
    ) as stream:
        async for event in stream:
            kube_state.update_piedata_status(status, event)


if __name__ == "__main__":
    # Listen socket.
    env_listen_host = getenv("PIECUT_BIND_HOST", "127.0.0.1")
    env_listen_port = int(getenv("PIECUT_BIND_PORT", 8000))

    # Namespace to work with.
    env_namespace = getenv("PIECUT_NAMESPACE")
    if env_namespace is None:
        logger.fatal("Environment variable PIECUT_NAMESPACE is empty, must be set")
        sys.exit(1)

    # Instance name for PieSec
    instance_name = getenv("PIECUT_INSTANCE_NAME")
    if instance_name is None:
        logger.fatal("Environment variable PIECUT_INSTANCE_NAME is empty, must be set")
        sys.exit(1)

    # External URL for paths.
    env_external_url = getenv("PIECUT_EXTERNAL_URL")
    if env_external_url is None:
        logger.fatal("Environment variable PIECUT_EXTERNAL_URL is empty, must be set")
        sys.exit(1)

    env_piedata_hostname_claim = getenv("PIECUT_PIEDATA_HOSTNAME_OVERRIDE_CLAIM")

    external_url = url_parse.urlparse(env_external_url)
    # Define path as '/' if empty.
    # Needed to generate proper Ingress.
    if external_url.path == "":
        external_url = url_parse.ParseResult(
            path="/",
            fragment=external_url.fragment,
            netloc=external_url.netloc,
            params=external_url.params,
            query=external_url.query,
            scheme=external_url.scheme,
        )

    # Extra PieData values.
    env_extra_piedata_values_file = getenv("PIECUT_EXTRA_PIEDATA_VALUES_FILE")
    if env_extra_piedata_values_file is not None:
        with open(env_extra_piedata_values_file, "rt") as f:
            extra_piedata_values = yaml.safe_load(f)
    else:
        extra_piedata_values = {}

    env_debug_access_token = getenv("PIECUT_DEBUG_ACCESS_TOKEN", secrets.token_hex(32))

    static_files_path = getenv("PIECUT_STATIC_FILES_PATH", __CONST_STATIC_FILES_PATH)
    favicon_path = getenv("PIECUT_FAVICON_PATH", __CONST_STATIC_FAVICON_PATH)
    wait_loop_file = getenv("PIECUT_WAIT_LOOP_FILE", __CONST_STATIC_WAIT_LOOP_FILE)

    # Logging
    log_level = getenv("PIECUT_LOG_LEVEL", "INFO").upper()

    # Optional handlers switches
    kick_enabled = getenv_bool("PIECUT_HANDLER_KICK_ENABLED", False)
    kill_enabled = getenv_bool("PIECUT_HANDLER_KILL_ENABLED", False)

    # Extra Token Claims switch
    piedata_token_claims_json = getenv_bool("PIECUT_ADD_PIEDATA_TOKEN_JSON", False)

    logger.info("Starting with configuration")
    logger.info("listen_socket=tcp://%s:%s", env_listen_host, env_listen_port)
    logger.info("watch_namespace=%s", env_namespace)
    logger.info("instance_name=%s", instance_name)
    logger.info("external_url=%s", external_url.geturl())
    logger.info("extra_piedata_values_file=%s", env_extra_piedata_values_file)
    logger.info("debug_token=%s", env_debug_access_token)
    logger.info("static_files_path=%s", static_files_path)
    logger.info("favicon_path=%s", favicon_path)
    logger.info("wait_loop_file=%s", wait_loop_file)
    logger.info("log_level=%s", log_level)
    logger.info("optional_handler_kick_enabled=%s", kick_enabled)
    logger.info("optional_handler_kill_enabled=%s", kill_enabled)
    logger.info("include_token_claims_to_piedata=%s", piedata_token_claims_json)
    logger.info("piedata_hostname_override_claim=%s", env_piedata_hostname_claim)

    # Prepare and start event loop.
    loop = asyncio.get_event_loop()

    # Create web API.
    web_app, site = loop.run_until_complete(
        create_web_app(
            host=env_listen_host,
            port=env_listen_port,
            external_url=external_url,
            kube_namespace=env_namespace,
            instance_name=instance_name,
            debug_token=env_debug_access_token,
            static_files_path=static_files_path,
            favicon_path=favicon_path,
            wait_loop_file=wait_loop_file,
            extra_piedata_values=extra_piedata_values,
            kick_enabled=kick_enabled,
            kill_enabled=kill_enabled,
            log_level=log_level,
            piedata_token_claims_json=piedata_token_claims_json,
            hostname_claim=env_piedata_hostname_claim,
        )
    )

    gatherer = asyncio.gather(
        site.start(),
        run_kubernetes_piedata_watch(web_app=web_app, namespace=env_namespace),
    )

    try:
        # loop.run_forever()
        loop.run_until_complete(gatherer)
    except KeyboardInterrupt:
        logger.info("Got CTRL-C, stopping.")
