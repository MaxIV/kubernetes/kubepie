# ApachePie

Apache 2.4 web server container with additional components which provide OpenID and OAuth2.0
authentication and authorization support.

Extra components:

- [Zmartzone Mod Auth OpenID](https://github.com/zmartzone/mod_auth_openidc/)
- [Zmartzone Mod OAuth2](https://github.com/zmartzone/mod_oauth2)
