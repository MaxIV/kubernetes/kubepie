# PieData

### Overview

PieData is an Apache2 web server configured to server data for one user with the user's access UID, GIDs.

PieData uses a Apache2 container image with OIDC authentication module installed.

PieData's Kubernetes Pod has an extra container [PieTrack](../../PieTrack)
which parses Apache2 access log and produces:

* Apache2 access log based metrics - `GET /metrics`
* Activity status of the Pod i.e. if Apache2 has had requests from the user recently `GET /is-active`

The PieTrack has 180 seconds default `is-active` value which can be configured via environment variables.

## PieData CRD

The PieData CRD is hard-coded in several places, so if you need to change it, do it simultaneously. The places are:

1. [PieDeck Operator config](piedeck/config/crd/bases/kubepie.io_piedata.yaml)
2. [PieDeck Operator helm chart](piedeck/operator-chart/piedeck/crds/kubepie.io_piedata.yaml)
3. [PieCut Python code](piecut/src/lib/piedata.py)

## PieData Helm Chart Values

After changing the PieData Chart's values,
go to PieDeck and run `make print-flattened-values`.

Use the values to update, if necessary:

* [PieDeck watches.yaml](piedeck/watches.yaml)
* [PieDeck operator-chart values](piedeck/operator-chart/piedeck/values.yaml) - chart templates and values which are used to configure watches.yaml.
* [PieDeck Piedata settings secret](piedeck/operator-chart/piedeck/templates/piedata/settings-secret.yaml)

Pay attention, that [the PieDeck chart values.yaml](piedeck/operator-chart/piedeck/values.yaml)
sets fixed tags for container images such as:

* curlimages/curl
* quay.io/maxiv/pietrack
* quay.io/maxiv/piepass

so don't forget to update them when necessary.

## PieData Activity Check

Piedata [Helm chart has a Service](piedeck/helm-charts/piedata/templates/service.yaml) which has a port `pietrack`.
The port connects HTTP clients to [piedata pietrack API](/src/python/kubepie/pietrack/wsgi.py).

If you change the Helm chart (Service name or port), change and rebuild
[PieEat constants PIEDATA_SERVICE_{NAME_FORMAT, PORT}](/src/python/pieeat.py) in PieEat.

Also, if you change the PieTrack API for `is-active` reflect it in [/src/python/pieeat.py](/src/python/pieeat.py) file.
