import logging
import sys

from gunicorn.app.wsgiapp import WSGIApplication
from gunicorn.glogging import Logger as GLogger
from kubepie.common.getenv import getenv

logger = logging.getLogger('KubePie.WSGI')


class WSGILogger(GLogger):
    def __init__(self, cfg):
        super().__init__(cfg)
        self.error_log = logging.getLogger("KubePie.WSGI.ServerLog")
        self.access_log = logging.getLogger("KubePie.WSGI.AccessLog")


class WSGI(WSGIApplication):
    def __init__(self, app_uri, tls=False, options=None):
        self.options = options or {}
        self.app_uri = app_uri
        self.tls = tls
        super().__init__()

    def load_config(self):
        self.cfg.set('logger_class', WSGILogger)
        self.cfg.set('accesslog', '-')
        # define bind port based on environment
        default_http_port = 8443 if self.tls else 5000
        http_port = int(getenv('HTTP_PORT', default_http_port))
        self.cfg.set('bind', ['0.0.0.0:{}'.format(http_port)])
        # handle TLS
        if self.tls:
            tls_cert = getenv('TLS_CERT', None)
            tls_key = getenv('TLS_KEY', None)

            if tls_cert is None or tls_key is None:
                logger.error("TLS key/cert are not defined. Exiting.")
                sys.exit(1)

            self.cfg.set('certfile', tls_cert)
            self.cfg.set('keyfile', tls_key)
        # optional config options passed to constructor
        for key, value in self.options.items():
            if key in self.cfg.settings:
                self.cfg.set(key, value)
