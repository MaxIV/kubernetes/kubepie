import logging
import os
import sys

from gunicorn import \
    SERVER_SOFTWARE  # apart from logging this makes it to requirements.txt
from kubepie.common.wsgi import WSGI
from kubepie.piesec.controller import admission_controller

logger = logging.getLogger('KubePie.PieSec.WSGI')


def app():
    """Return web-app with configuration from ENV applied"""
    # controller permissive mode
    if 'PERMISSIVE' in os.environ is not None:
        admission_controller.config['PERMISSIVE'] = True

    # config files
    config_files = []
    if 'CONFIG_FILE' in os.environ:
        config_files.append(os.getenv('CONFIG_FILE'))
    if 'SECRET_FILE' in os.environ:
        config_files.append(os.getenv('SECRET_FILE'))
    admission_controller.config['MAP_CONFIG_FILES'] = config_files

    # mapping plugin
    admission_controller.config['MAP_PLUGIN'] = os.getenv(
        'MAP_PLUGIN', 'static')

    return admission_controller


def run():
    """Start controller web-server"""
    # start dev environemnt with Debug logging
    if logger.getEffectiveLevel() == logging.DEBUG:
        logger.info('Staring PieSec using Flask WSGI server in debug mode')
        https_port = int(os.getenv('HTTP_PORT', 8443))
        tls_cert = os.getenv('TLS_CERT', None)
        tls_key = os.getenv('TLS_KEY', None)

        if tls_cert is None or tls_key is None:
            logger.error("TLS key/cert are not defined. Exiting.")
            sys.exit(1)
        app().run(host='0.0.0.0', port=https_port,
                  ssl_context=(tls_cert, tls_key),
                  debug=True)
    else:
        # production WSGI run
        logger.info('Starting PieSec using %s', SERVER_SOFTWARE)
        WSGI("kubepie.piesec.wsgi:app()", tls=True).run()
