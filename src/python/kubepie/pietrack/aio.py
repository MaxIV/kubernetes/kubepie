import logging
import os
import signal
import sys
from datetime import timedelta
from time import sleep
from typing import Optional

from aiohttp import web
from kubepie.common.getenv import getenv, getenv_bool
from kubepie.common.piecrust import PIETRACK_DEFAULT_IS_ACTIVE_TIMEOUT
from prometheus_client import REGISTRY, CollectorRegistry, generate_latest, multiprocess

from .apache_log import ApacheLogParser
from .logmetrics import SharedLogMetrics
from .walltime import WallTimeMetrics

CONST_WEB_APP_STORAGE_LOG_METRICS = "pietrack_log_metrics"
CONST_WEB_APP_WALLTIME_METRICS = "pietrack_walltime_metrics"
CONST_WEB_APP_USERINFO_METRICS = "pietrack_userinfo_metrics"
CONST_WEB_APP_STORAGE_METRICS_REGISTRY = "pietrack_registry"

CONST_ENV_METRICS_LOG_LEVEL = "METRICS_LOG_LEVEL"
CONST_ENV_METRICS_STARTUP_DELAY_SEC = "METRICS_STARTUP_DELAY_SEC"
CONST_ENV_METRICS_MULTIPROCESS_TMP_PATH = "METRICS_MULTIPROCESS_COLLECTOR_PATH"
CONST_ENV_METRICS_HTTP_LISTEN_PORT = "METRICS_HTTP_PORT"
CONST_ENV_METRICS_HTTP_LISTEN_HOST = "METRICS_HTTP_HOST"


CONST_ENV_METRICS_LOGS_INCLUDE_VHOSTS = "METRICS_LOGS_INCLUDE_VHOSTS"
CONST_ENV_METRICS_LOGS_EXCLUDE_VHOSTS = "METRICS_LOGS_EXCLUDE_VHOSTS"
CONST_ENV_METRICS_LOGS_IS_ACTIVE_TIMEOUT_SEC = "METRICS_LOGS_IS_ACTIVE_TIMEOUT_SEC"
CONST_ENV_METRICS_LOGS_OVERRIDE_USERNAME = "METRICS_LOGS_OVERRIDE_CLIENT_USERNAME"
CONST_ENV_METRICS_LOGS_INPUT_FILE = "METRICS_LOGS_INPUT_LOGFILE"
CONST_ENV_METRICS_WALLTIME_MAX_SEC = "METRICS_WALLTIME_MAX_SEC"
CONST_ENV_METRICS_WALLTIME_ENABLED = "METRICS_WALLTIME_ENABLED"
CONST_ENV_METRICS_USER_INFO_ENABLED = "METRICS_USER_INFO_ENABLED"


logger = logging.getLogger("KubePie.PieTrack.WebApp")


def init_log_metrics(registry: CollectorRegistry) -> SharedLogMetrics:
    # log stream source
    env_input_log = getenv(CONST_ENV_METRICS_LOGS_INPUT_FILE, "")
    if env_input_log is None:
        logs_stream = sys.stdin
    else:
        logs_stream = open(env_input_log, "rt")

    # Apache log metrics
    env_include_vhosts = getenv(CONST_ENV_METRICS_LOGS_INCLUDE_VHOSTS)
    env_exclude_vhosts = getenv(CONST_ENV_METRICS_LOGS_EXCLUDE_VHOSTS)

    if env_include_vhosts is None:
        include_vhosts = None
    else:
        include_vhosts = env_include_vhosts.strip().split(",")

    if env_exclude_vhosts is None:
        exclude_vhosts = None
    else:
        exclude_vhosts = env_exclude_vhosts.strip().split(",")

    # is_active timeout
    env_is_active_timeout = getenv(
        CONST_ENV_METRICS_LOGS_IS_ACTIVE_TIMEOUT_SEC, PIETRACK_DEFAULT_IS_ACTIVE_TIMEOUT
    )
    is_active_timeout = timedelta(seconds=int(env_is_active_timeout))

    # Client username override
    env_client_username_override = getenv(
        CONST_ENV_METRICS_LOGS_OVERRIDE_USERNAME, None
    )

    env_user_info_enable = getenv_bool(CONST_ENV_METRICS_USER_INFO_ENABLED, False)

    logger.info(
        (
            "Starting Apache metrics with input_log=%s, "
            "include_vhosts=%s, "
            "exclude_vhosts=%s, "
            "is_active_timeout=%s, "
            "log_level=%s, "
            "client_username_override=%s, "
            "client_user_info_enable=%s"
        ),
        env_input_log if env_input_log is not None else "STDIN",
        include_vhosts,
        exclude_vhosts,
        is_active_timeout,
        logger.getEffectiveLevel(),
        env_client_username_override,
        env_user_info_enable,
    )

    logs_parser = ApacheLogParser(
        log_stream=logs_stream,
        include_vhosts=include_vhosts,
        exclude_vhosts=exclude_vhosts,
    )

    logger.info("Starting Apache log metrics collector")

    metrics = SharedLogMetrics(
        requests=logs_parser,
        default_is_active_timeout=is_active_timeout,
        registry=registry,
        client_username_override=env_client_username_override,
        enable_user_info=env_user_info_enable,
    )
    metrics.start()

    # Custom signals handler to stop threads.
    original_handler = signal.getsignal(signal.SIGINT)

    def sigint_handler(signum, frame):
        logger.info("Stopping background runners: log_parser, metrics")

        logs_parser.stop()
        metrics.stop()

        original_handler(signum, frame)  # type: ignore

    try:
        signal.signal(signal.SIGINT, sigint_handler)
    except ValueError as e:
        logging.error(f"{e}. Continuing execution...")

    return metrics


def init_walltime_metrics(registry: CollectorRegistry) -> WallTimeMetrics:
    max_walltime = getenv(CONST_ENV_METRICS_WALLTIME_MAX_SEC, None)

    if max_walltime is None:
        raise KeyError(
            f"Max Walltime is enabled but environment variable {CONST_ENV_METRICS_WALLTIME_MAX_SEC} is not defined"
        )

    logger.info("Starting Walltime with max time seconds=%s", max_walltime)

    walltime = WallTimeMetrics(registry=registry, max_walltime_sec=int(max_walltime))
    walltime.start()

    # Custom signals handler to stop threads.
    original_handler = signal.getsignal(signal.SIGINT)

    def sigint_handler(signum, frame):
        logger.info("Stopping background runners: walltime")

        walltime.stop()

        original_handler(signum, frame)  # type: ignore

    try:
        signal.signal(signal.SIGINT, sigint_handler)
    except ValueError as e:
        logging.error(f"{e}. Continuing execution...")

    return walltime


async def handler_get_metrics(request: web.Request) -> web.Response:
    data = generate_latest(request.app[CONST_WEB_APP_STORAGE_METRICS_REGISTRY])

    # content_type is prometheus_client.CONTENT_TYPE_LATEST
    # but without charset as aiohttp raises error if charset is in
    # Content-Type.
    response = web.Response(body=data, content_type="text/plain; version=0.0.4;")

    return response


async def handler_is_active(request: web.Request) -> web.Response:
    metrics: SharedLogMetrics = request.app[CONST_WEB_APP_STORAGE_LOG_METRICS]

    alive = metrics.is_active()

    try:
        walltime: WallTimeMetrics = request.app[CONST_WEB_APP_WALLTIME_METRICS]

        walltime_act = walltime.is_active()

        alive.TTL_left = min(alive.TTL_left, walltime_act.TTL_left)
    except KeyError:
        pass

    return web.json_response(
        data={
            "is_active": alive.is_active,
            "ttl": alive.TTL_left,
        }
    )


def create_app(
    registry: CollectorRegistry,
    metrics: SharedLogMetrics,
    walltime: Optional[WallTimeMetrics] = None,
) -> web.Application:
    app = web.Application(logger=logging.getLogger("KubePie.PieTrack.WebApp.Access"))

    aio_log = logging.getLogger("aiohttp.access")
    aio_log.setLevel(getenv(CONST_ENV_METRICS_LOG_LEVEL, "INFO").upper())
    log_handler_stdout = logging.StreamHandler(stream=sys.stdout)
    aio_log.addHandler(log_handler_stdout)

    app[CONST_WEB_APP_STORAGE_METRICS_REGISTRY] = registry

    app[CONST_WEB_APP_STORAGE_LOG_METRICS] = metrics

    if walltime is not None:
        app[CONST_WEB_APP_WALLTIME_METRICS] = walltime

    app.add_routes(
        (
            web.get(
                path="/metrics",
                handler=handler_get_metrics,
                name="metrics",
            ),
            web.get(
                path="/is-active",
                handler=handler_is_active,
                name="is_active",
            ),
        )
    )

    return app


def run_blocking():
    # Artificial delay to accommodate Apache startup delay.
    startup_delay = int(getenv(CONST_ENV_METRICS_STARTUP_DELAY_SEC, 3))
    logger.info("Waiting for %s seconds before metrics collection", startup_delay)
    sleep(startup_delay)

    # Prepare Prometheus client
    # Run multiprocess Prometheus metrics exporter.
    prom_dir = getenv(
        CONST_ENV_METRICS_MULTIPROCESS_TMP_PATH, "/tmp/prometheus_metrics"
    )
    os.makedirs(prom_dir, exist_ok=True)
    multiprocess.MultiProcessCollector(registry=REGISTRY, path=prom_dir)

    log_metrics = init_log_metrics(REGISTRY)

    walltime = None
    if getenv_bool(CONST_ENV_METRICS_WALLTIME_ENABLED, False):
        walltime = init_walltime_metrics(registry=REGISTRY)

    app = create_app(
        registry=REGISTRY,
        metrics=log_metrics,
        walltime=walltime,
    )

    http_port = int(os.getenv(CONST_ENV_METRICS_HTTP_LISTEN_PORT, 5000))
    http_host = os.getenv(CONST_ENV_METRICS_HTTP_LISTEN_HOST, "0.0.0.0")

    logger.info("Starting PieTrack on {}:{}".format(http_host, http_port))

    web.run_app(app, host=http_host, port=http_port)


if __name__ == "__main__":
    run_blocking()
