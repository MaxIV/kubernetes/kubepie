{{/*
* TODO: make it common in Kubepie charts? generate with python helper based on definitions?
*/}}

{{/*
KubePie Domain definition
*/}}
{{- define "kubepie.domain" -}}
kubepie.io
{{- end -}}


{{/*
PieSec Pod Instance label (triggers admission controller webhook)
*/}}
{{- define "piesec.instance.label" -}}
{{- printf "piesec.%s/instance" ( include "kubepie.domain" . ) -}}
{{- end -}}