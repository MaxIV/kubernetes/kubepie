import logging
import threading
import typing
from datetime import datetime, timedelta
from http import HTTPStatus
from multiprocessing import Manager

from prometheus_client import CollectorRegistry, Counter, Gauge

from .common import ClientRequest, ActivityStatus


class SharedLogMetrics:
    __metric_labels_user_info = [
        "client_username",
    ]
    __metric_labels_request_info = __metric_labels_user_info + [
        "client_hostname",
        "request_method",
        "virtual_host",
    ]
    __metric_labels_last_activity = __metric_labels_user_info + [
        "client_hostname",
        "virtual_host",
    ]

    # Multiprocessing Manager dict keys
    __state_running = "is_running"
    __state_requests_count = "requests_count"
    __state_last_access = "last_access"

    __ignored_statuses = {
        HTTPStatus.UNAUTHORIZED,
        HTTPStatus.FORBIDDEN,
    }

    def __init__(
        self,
        requests: typing.Iterator[ClientRequest],
        default_is_active_timeout: timedelta,
        registry: CollectorRegistry,
        enable_user_info: bool,
        client_username_override: typing.Optional[str] = None,
    ) -> None:
        self._requests = requests
        # Init last access with current time so that
        # is_active returns True for at least default_is_active_timeout
        # after the start of the LogMetrics.
        self.__default_is_active_tm = default_is_active_timeout
        self.logger = logging.getLogger("KubePie.PieTrack.LogMetrics")

        th = threading.Thread(target=self.__update_metrics)
        self.__th = th

        # Shared memory state.
        self.__manager = Manager()

        # Log based Prometheus metrics
        self.__metrics_requests = Counter(
            "piedata_http_response_status",
            "Number of HTTP responses by status",
            labelnames=self.__metric_labels_request_info + ["response_status"],
            registry=registry,
        )
        self.__metrics_bytes = Counter(
            "piedata_http_response_bytes",
            "Total number of sent bytes",
            labelnames=self.__metric_labels_request_info,
            registry=registry,
        )

        self.__metrics_last_activity = Gauge(
            "piedata_http_last_activity",
            "Last activity timestamp",
            labelnames=self.__metric_labels_last_activity,
            registry=registry,
        )

        # Redefine username in metrics
        self._client_username_override = client_username_override

        # Activity state.
        self.__activity = self.__manager.dict(
            {
                self.__state_requests_count: 0,
                self.__state_last_access: datetime.now().astimezone(),
                self.__state_running: False,
            }
        )

        self.__enable_user_info = enable_user_info
        if enable_user_info:
            # User info metric. Value is always 1 and is supposed to be used
            # with Prometheus "group_left", "on", etc operators
            # to enrich metrics coming from container runtimes with the
            # user info.
            self.__userinfo = Gauge(
                "piedata_user_info",
                "User information",
                labelnames=self.__metric_labels_user_info,
                registry=registry,
            )

    def start(self):
        self.__activity[self.__state_running] = True
        self.__th.start()

    def stop(self):
        self.__activity[self.__state_running] = False
        self.__th.join()

    def __update_metrics(self):
        """Loads requests from requests iterator and updates metrics"""
        self.logger.info("Starting metrics update thread")

        for req in self._requests:
            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug("Parsed request: %s", req)

            if not self.__activity[self.__state_running]:
                break

            # Ignore requests with Forbidden and Unauthorized codes
            # as they do not show a real user activity.
            if req.response_status in self.__ignored_statuses:
                continue

            self.__activity[self.__state_last_access] = req.request_timestamp
            self.__activity[self.__state_requests_count] += 1  # type: ignore

            if self._client_username_override is None:
                client_username = req.client_username
            else:
                client_username = self._client_username_override

            self.__metrics_bytes.labels(
                client_username=client_username,
                client_hostname=req.client_hostname,
                request_method=req.request_method,
                virtual_host=req.virtual_host,
            ).inc(req.sent_bytes)

            self.__metrics_requests.labels(
                client_username=client_username,
                client_hostname=req.client_hostname,
                request_method=req.request_method,
                virtual_host=req.virtual_host,
                response_status=req.response_status,
            ).inc()

            self.__metrics_last_activity.labels(
                client_username=client_username,
                client_hostname=req.client_hostname,
                virtual_host=req.virtual_host,
            ).set(req.request_timestamp.timestamp())

            if self.__enable_user_info:
                self.__userinfo.labels(client_username).set(1)

    def is_active(self) -> ActivityStatus:
        """Checks if the logs report that the log source is still active"""
        if not self.__activity[self.__state_running]:
            return ActivityStatus(is_active=False, TTL_left=0)

        last_access: typing.Optional[datetime] = self.__activity[
            self.__state_last_access
        ]  # type: ignore

        if last_access is None:
            return ActivityStatus(is_active=False, TTL_left=0)

        now = datetime.now().astimezone()
        # How many seconds are left to be active.
        ttl = int((last_access + self.__default_is_active_tm - now).total_seconds())

        if self.logger.isEnabledFor(logging.DEBUG):
            self.logger.debug(
                "now: %s, last_access: %s, ttl: %s", now, last_access, ttl
            )

        return ActivityStatus(is_active=(ttl >= 0), TTL_left=ttl)
