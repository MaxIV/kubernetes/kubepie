{{/*
Expand the name of the chart.
The default .Chart.Name is replaced with "piepass" to make the value
permanent for other charts which might include and use this template.
*/}}
{{- define "piepass.name" -}}
{{- default "piepass" .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "piepass.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "piepass.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common annotations
*/}}
{{- define "piepass.annotations" -}}
{{- with (coalesce .Values.global.extraAnnotations .Values.extraAnnotations) -}}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "piepass.labels" -}}
helm.sh/chart: {{ include "piepass.chart" . }}
{{ include "piepass.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "piepass.selectorLabels" -}}
app.kubernetes.io/name: {{ include "piepass.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- $labels := (coalesce .Values.global.extraLabels .Values.extraLabels) }}
{{- if $labels }}
  {{- range $k, $v := $labels }}
{{ $k }}: {{ $v }}
  {{- end }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "piepass.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "piepass.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Construct the image to use
*/}}
{{- define "piepass.image" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.image.repository $tag -}}
{{- end -}}
{{- end -}}

{{/*
Construct the image to use
*/}}
{{- define "piepass.pieTrackImage" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.monitoring.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.monitoring.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.monitoring.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.monitoring.image.repository $tag -}}
{{- end -}}
{{- end -}}
