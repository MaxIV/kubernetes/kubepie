import threading
import logging
import time
from datetime import datetime, timedelta

from prometheus_client import CollectorRegistry, Counter, Gauge

from .common import ActivityStatus


class WallTimeMetrics:
    __UPDATE_INTERVAL_SEC = 5

    def __init__(self, registry: CollectorRegistry, max_walltime_sec: int):
        self.logger = logging.getLogger("KubePie.PieTrack.WallTime")

        self.__th = threading.Thread(target=self.__update_metrics)

        self.__running = False

        self.__metrics_time_left = Gauge(
            "piedata_walltime_left",
            "Number of seconds left before shutdown",
            registry=registry,
        )

        self.__max_walltime = max_walltime_sec
        self.__start = None
        self.__time_left = self.__max_walltime

    def __update_metrics(self):
        self.__metrics_time_left.set(self.__max_walltime)

        while self.__running:
            time.sleep(self.__UPDATE_INTERVAL_SEC)

            now = datetime.now()
            delta = now - self.__start
            seconds = delta.total_seconds()
            left = self.__max_walltime - seconds
            self.__time_left = left

            self.logger.debug("Update metrics step, remaining time: %s", left)

            self.__metrics_time_left.set(left)

    def is_active(self) -> ActivityStatus:
        if not self.__running:
            return ActivityStatus(is_active=False, TTL_left=0)

        return ActivityStatus(
            is_active=(self.__time_left >= 0), TTL_left=self.__time_left
        )

    def start(self):
        self.__start = datetime.now()
        self.__time_left = self.__max_walltime
        self.__running = True
        self.__th.start()

    def stop(self):
        self.__start = None
        self.__running = False
        self.__th.join()
