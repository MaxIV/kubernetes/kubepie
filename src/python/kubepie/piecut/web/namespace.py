from aiohttp import web

# Key to store current namespace for PieCut.
CONST_PIECUT_NAMESPACE_KEY = 'kubernetes_namespace'


def namespace_set(app: web.Application, ns: str):
    '''Stores current Kubernetes namespace in Application context.'''
    app[CONST_PIECUT_NAMESPACE_KEY] = ns


def namespace_get(request: web.Request) -> str:
    '''Retrieves current Kubernetes namespace from Application context.'''
    return request.app[CONST_PIECUT_NAMESPACE_KEY]
