{{/*
Expand the name of the chart.
*/}}
{{- define "pieeat.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "pieeat.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "pieeat.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common annotations
*/}}
{{- define "pieeat.annotations" -}}
{{- with (coalesce .Values.global.extraAnnotations .Values.extraAnnotations) -}}
{{ toYaml . }}
{{- end }}
{{- end }}


{{/*
Common labels
*/}}
{{- define "pieeat.labels" -}}
helm.sh/chart: {{ include "pieeat.chart" . }}
{{ include "pieeat.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "pieeat.selectorLabels" -}}
app.kubernetes.io/name: {{ include "pieeat.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- with (coalesce .Values.global.extraLabels .Values.extraLabels) }}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "pieeat.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "pieeat.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Construct the image to use
*/}}
{{- define "pieeat.image" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.image.repository $tag -}}
{{- end -}}
{{- end -}}