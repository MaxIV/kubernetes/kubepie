from dataclasses import dataclass


@dataclass
class OIDCUserInfo:
    '''Contains information extracted from Apache OIDC provided headers'''
    # access_token: str
    audience: str
    email: str
    family_name: str
    given_name: str
    name: str
    issuer: str
    preferred_username: str
    sub: str
    token_claims_json: str
