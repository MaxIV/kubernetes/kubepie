import asyncio
import json
import logging
from datetime import datetime, timezone
from typing import Optional

import aiohttp
import kopf
from kubepie.common.getenv import getenv
from kubepie.common.kube_state import PieDataState, PieDataStateType
from kubepie.common.piecrust import (
    PIEDATA_K8S_GROUP,
    PIEDATA_K8S_KIND_PLURAL,
    PIEDATA_K8S_VERSION,
)
from kubernetes_asyncio import client, config

# Initial configuration parameters
# Interval to perform is-active checks to the deployed PieData.
CHECK_INTERVAL_SEC = int(getenv("PIEEAT_CHECK_INTERVAL_SEC", 15))
# Initial delay before starting the activity checks.
INITIAL_DELAY_SEC = int(getenv("PIEEAT_INITIAL_DELAY_SEC", 5))
# Time from the moment of creation after which the failed PieData
# resources will be deleted.
FAILED_RELEASES_DELETE_AFTER = int(getenv("PIEEAT_DELETE_FAILED_AFTER_SEC", 5))
# Time to wait for PieDeck to set PieData.status field and start working
# with a PieData.
# If PieDeck does not manage to start working after this time,
# the PieData is deleted.
PIEDECK_WAIT_TIMEOUT = int(getenv("PIEDECK_WAIT_TIMEOUT", 120))
# Namespace to work with.
PIEEAT_NAMESPACE = getenv("PIEEAT_NAMESPACE")
LOG_METRICS_FAILURE_COUNT_THRESHOLD = int(getenv("PIEEAT_MONITOR_FAILURE_COUNT", 5))
LIVENESS_PORT = getenv("PIEEAT_LIVENESS_PORT", 8080)


PIEDATA_SERVICE_NAME_FORMAT = getenv(
    "PIEDATA_SERVICE_NAME_TEMPLATE", "{piedata_name}-piedata"
)
PIEDATA_SERVICE_PORT = int(getenv("PIEDATA_SERVICE_CHECK_PORT", 2118))


@kopf.daemon("piedata")  # type: ignore
async def monitor_piedata(stopped: bool, meta, status, **kwargs):
    namespace = meta["namespace"]
    name = meta["name"]

    logger = logging.getLogger(
        "KubePie.PieEat.kopf.piedata.{namespace}.{name}".format(
            namespace=namespace, name=name
        )
    )

    logger.info("Waiting for not empty .status to be set by PieDeck")
    try:
        await __wait_for_status(
            logger=logger, status=status, timeout_sec=PIEDECK_WAIT_TIMEOUT
        )
    except TimeoutError as e:
        logger.error("%s: deleting PieData", e)
        await delete_piedata(namespace=namespace, name=name, logger=logger)
        return

    logger.info(
        (
            "Starting monitoring daemon, waiting for initial_delay=%s seconds "
            "before starting is-active check"
        ),
        INITIAL_DELAY_SEC,
    )
    await asyncio.sleep(INITIAL_DELAY_SEC)
    logger.info("Starting is-active checks")

    failure_count = 0
    service_name = PIEDATA_SERVICE_NAME_FORMAT.format(piedata_name=meta["name"])

    is_active_url = "http://{service_name}:{port}/is-active".format(
        service_name=service_name, port=PIEDATA_SERVICE_PORT
    )

    async with aiohttp.ClientSession() as aiohttp_session:
        while not stopped:
            state = PieDataState.from_status(status)

            # Deployed PieData - check activity.
            if state.type_ == PieDataStateType.ConditionDeployed:
                # Check activity.
                try:
                    async with aiohttp_session.get(
                        is_active_url, raise_for_status=True
                    ) as response:
                        # Check the is-active status.
                        if logger.isEnabledFor(logging.DEBUG):
                            body = await response.read()
                            logger.debug("Raw response: %s", body)

                            json_resp = json.loads(body)
                            logger.debug("JSON response: %s", json_resp)
                        else:
                            json_resp = await response.json()

                        # JSON response should be:
                        # {
                        #     'is_active': alive.is_active, # boolean
                        #     'ttl': alive.TTL_left, # Time-to-live seconds
                        # }
                        is_active = json_resp["is_active"]
                        ttl = json_resp["ttl"]

                        logger.info(
                            "is-active check for %s: is-active=%s, ttl=%s",
                            is_active_url,
                            is_active,
                            ttl,
                        )

                        if is_active is False:
                            logger.info("PieData is inactive, preparing to delete")
                            await delete_piedata(
                                namespace=namespace, name=name, logger=logger
                            )

                        # Check success.
                        failure_count = 0
                except aiohttp.ClientError as e:
                    failure_count += 1
                    await handle_check_exception(failure_count, meta, e, logger)
                except KeyError as e:
                    failure_count += 1
                    await handle_check_exception(failure_count, meta, e, logger)
                except Exception as e:
                    failure_count += 1
                    await handle_check_exception(failure_count, meta, e, logger)

            # Failed to deploy - wait timeout and delete.
            elif state.type_ in {
                PieDataStateType.ConditionIrreconcilable,
                PieDataStateType.ConditionReleaseFailed,
                PieDataStateType.ConditionUnknown,
            }:
                # Delete the release if it is old.
                creation_ts_raw = meta["creationTimestamp"]
                creation_ts = datetime.fromisoformat(creation_ts_raw)

                now = datetime.now(timezone.utc).astimezone()
                createdAgo = (now - creation_ts).total_seconds()

                logger.info(
                    (
                        "Release condition %s, a candidate for removal. "
                        "Created %s seconds ago"
                    ),
                    state.type_,
                    createdAgo,
                )

                if createdAgo > FAILED_RELEASES_DELETE_AFTER:
                    logger.info(
                        "Release condition %s, created %s seconds ago",
                        state.type_,
                        createdAgo,
                    )

                    await delete_piedata(meta["namespace"], meta["name"], logger)

            # Everything else - do nothing
            else:
                logger.debug("state.type_ is %s, do nothing", state.type_)

            await asyncio.sleep(CHECK_INTERVAL_SEC)


async def delete_piedata(
    namespace: Optional[str], name: Optional[str], logger: logging.Logger
):
    if namespace is None:
        raise ValueError("namespace cannot be None")

    logger.info("Deleting PieData")

    async with client.ApiClient() as api:
        try:
            api_client = client.CustomObjectsApi(api)
            await api_client.delete_namespaced_custom_object(
                group=PIEDATA_K8S_GROUP,
                version=PIEDATA_K8S_VERSION,
                namespace=namespace,
                plural=PIEDATA_K8S_KIND_PLURAL,
                name=name,
            )

        except client.ApiException as e:
            if e.status == 404:
                logger.info("PieData not found - no need to delete")
            else:
                # Make it fail intentionally
                # to show that something is wrong.
                raise e


async def handle_check_exception(failure_count: int, meta, exc, logger: logging.Logger):
    # Failure to check metrics.
    logger.error(
        ("Failure to perform is-active check " "(failure count=%s/%s): %s"),
        failure_count,
        LOG_METRICS_FAILURE_COUNT_THRESHOLD,
        exc,
    )

    # Delete if the number of failures is more than the
    # threshold.
    if failure_count > LOG_METRICS_FAILURE_COUNT_THRESHOLD:
        logger.error(
            (
                "%s consequential failures to check status. "
                "Assuming the PieData is dead. "
                "Deleting PieData."
            ),
            failure_count,
        )
        await delete_piedata(meta["namespace"], meta["name"], logger)


async def init_kube_client():
    logger = logging.getLogger(__name__)

    logger.info("Loading Kubernetes configuration.")

    try:
        await config.load_kube_config()
    except config.ConfigException as e:
        logger.info("Cannot configure kube client via KUBECONFIG: %s", e)
        logger.info("Trying Kubernetes in-cluster config")

        config.load_incluster_config()


async def __wait_for_status(logger: logging.Logger, status, timeout_sec: int):
    __INTERVAL = 5

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(
            "StatusLoop: Starting wait for not empty .status, status: %s", status
        )

    while status is None or len(status) == 0:
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("StatusLoop: Time left %s sec", timeout_sec)

        timeout_sec -= __INTERVAL

        if timeout_sec <= 0:
            raise TimeoutError("Timeout waiting for not empty .status in PieData")

        await asyncio.sleep(__INTERVAL)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("StatusLoop: PieData has not empty .status: Stop waiting")
