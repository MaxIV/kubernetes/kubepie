from .common import PieSecConfigException, UserMap
from .staticmap import StaticUserMap
from .ldapmap import LDAPUserMap

PIESEC_PLUGINS = {
    'static': StaticUserMap,
    'ldap': LDAPUserMap
}
