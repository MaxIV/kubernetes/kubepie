from dataclasses import asdict, is_dataclass
from datetime import datetime
from json import JSONEncoder as _Encoder
from typing import Any

from kubepie.common import kube_state


class JSONEncoder(_Encoder):
    def default(self, o: Any):
        if isinstance(o, datetime):
            return o.isoformat()

        if isinstance(o, kube_state.PieDataStateType):
            return str(o.value)

        if is_dataclass(o):
            return asdict(o)

        return super().default(o)
