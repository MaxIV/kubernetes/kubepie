# KubePie

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kubepie)](https://artifacthub.io/packages/search?repo=kubepie)

**KubePie** is a framework for Kubernetes that aims to *serve just the right piece of data* for end-users over HTTP. 

*KubePie* facilitates streamlined access to end-user data by harnessing Kubernetes' scalability and deployment capabilities by running, managing and securing "personal" web servers for every user in the organization, using proper security context (user/group IDs).

Our "Pie" (Permissions Impersonated Environment) is baked from open-source components to provide a pleasant data consumption experience for end-users over the web, using *OpenID Connect* flow or *OAuth2 bearer tokens*.

All "Pies" are custom-made for each client to deliver an impersonated experience.

The microservices framework within *KubePie* is structured around various "Pies":

| **Component** | **Container Image** <div style="width:130px"/> | **Helm Chart** <div style="width:130px"/> | **Description** |
|---|---|---|---|
| PieData | [![Container Image](https://img.shields.io/badge/Container%20Image-piedata-green.svg)](https://quay.io/repository/maxiv/piedata?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/piedata)](https://artifacthub.io/packages/search?repo=piedata) | The personal web-server Pie, serving the data. Wrapped in a Helm chart. |
| PieTrack | [![Container Image](https://img.shields.io/badge/Container%20Image-pietrack-green.svg)](https://quay.io/repository/maxiv/pietrack?tab=tags) |  | Transfer activities keeping running along with *PieData*. Tracks your calories consumption over time. |
| PiePass | [![Container Image](https://img.shields.io/badge/Container%20Image-piepass-green.svg)](https://quay.io/repository/maxiv/piepass?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/piepass)](https://artifacthub.io/packages/search?repo=piepass) | Ensure secure passage to the KubePie bakery services for the customers. |
| PieCut | [![Container Image](https://img.shields.io/badge/Container%20Image-piecut-green.svg)](https://quay.io/repository/maxiv/piecut?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/piecut)](https://artifacthub.io/packages/search?repo=piecut) | The "Control Unit" of KubePie bakery, taking orders and delivering *PieDatas*. |
| PieDeck | [![Container Image](https://img.shields.io/badge/Container%20Image-piedeck-green.svg)](https://quay.io/repository/maxiv/piedeck?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/piedeck)](https://artifacthub.io/packages/search?repo=piedeck) | The "Data Endpoints Controller for Kubernetes" operates the baking process of the *PieDatas* for end-users. |
| PieSec | [![Container Image](https://img.shields.io/badge/Container%20Image-piesec-green.svg)](https://quay.io/repository/maxiv/piesec?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/piesec)](https://artifacthub.io/packages/search?repo=piesec) | Admission webhook for the security seasoning of the *PieData*. Adds the UID/GID and SGID spices. Make sure no rotten eggs are added. |
| PieEat | [![Container Image](https://img.shields.io/badge/Container%20Image-pieeat-green.svg)](https://quay.io/repository/maxiv/pieeat?tab=tags) | [![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/pieeat)](https://artifacthub.io/packages/search?repo=pieeat) | "Erases at Timeout" the stale *PieDatas*. |

Information about the installation and configuration of the KubePie can be found following the [KubePie Umbrella Helm Chart](https://artifacthub.io/packages/helm/kubepie/kubepie).

A description of the KubePie Technology Stack and additional content can be found in the [docs directory](./docs/).
