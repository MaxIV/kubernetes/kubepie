import logging
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Any, Dict, Optional, Tuple

logger = logging.getLogger("KubePie.KubeState")


class PieDataStateType(Enum):
    '''
    Statuses are from HelmAppConditionType from
    https://github.com/operator-framework/operator-sdk/blob/ee2e3fa8548fb4cd4945810609bcdd9531946ad6/internal/helm/internal/types/types.go#L58
    '''
    ConditionCreated = "Created"
    ConditionInitialized = "Initialized"
    ConditionDeployed = "Deployed"
    ConditionReleaseFailed = "ReleaseFailed"
    ConditionIrreconcilable = "Irreconcilable"
    ConditionUnknown = "Unknown"


PIEDATA_RELEASE_UNRECOVERABLE_STATES = {
    PieDataStateType.ConditionIrreconcilable,
    PieDataStateType.ConditionReleaseFailed}


@dataclass
class PieDataState:
    transition_time: Optional[datetime]
    message: Optional[str]
    reason: Optional[str]
    type_: PieDataStateType

    def __repr__(self) -> str:
        time_ = (self.transition_time.isoformat()
                 if self.transition_time else None)

        return (
            'lastTransitionTime={date_}, '
            'type={type_}, '
            'reason={reason}, '
            'message={message}').format(
            date_=time_,
            type_=self.type_,
            reason=self.reason,
            message=self.message)

    def dict(self) -> Dict[str, Any]:
        if self.transition_time is None:
            return {
                'lastTransitionTime': None,
                'type': self.type_.value,
                'reason': self.reason,
                'message': self.message,
            }

        return {
            'lastTransitionTime': self.transition_time.isoformat(),
            'type': self.type_.value,
            'reason': self.reason,
            'message': self.message,
        }

    @staticmethod
    def from_status(status: Dict) -> 'PieDataState':
        try:
            conditions = status['conditions']
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason='Event does not have .status.conditions field',
                type_=PieDataStateType.ConditionUnknown,
            )

        if len(conditions) == 0:
            return PieDataState(
                transition_time=None,
                message=None,
                reason='Event\'s .status.conditions list is empty',
                type_=PieDataStateType.ConditionUnknown,
            )

        last_state = conditions[-1]

        # Condition type.
        try:
            raw_type_ = last_state['type']
            # status = last_state['status']

            type_ = PieDataStateType.ConditionUnknown
            for v in PieDataStateType:
                if v.value == raw_type_:
                    type_ = v
                    break
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason=('Event\' .status[-1].condition does not contain '
                        '"type" field'),
                type_=PieDataStateType.ConditionUnknown,
            )

        # Message.
        try:
            message = last_state['message'].replace('\n', '; ')
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason=('Event\'s last .status.condition does not contain '
                        '"message" field'),
                type_=PieDataStateType.ConditionUnknown,
            )

        # Reason.
        try:
            reason = last_state['reason']
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason=('Event\' .status[-1].condition does not contain '
                        '"message" field'),
                type_=PieDataStateType.ConditionUnknown,
            )

        # Transition time.
        try:
            raw_time = last_state['lastTransitionTime']
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason=('Event\' .status[-1].condition does not contain '
                        '"lastTransitionTime" field'),
                type_=PieDataStateType.ConditionUnknown,
            )

        try:
            date = datetime.fromisoformat(raw_time)
        except ValueError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason='Event\' .status[-1].condition has wrong time format',
                type_=PieDataStateType.ConditionUnknown,
            )

        return PieDataState(
            transition_time=date,
            message=message,
            reason=reason,
            type_=type_,
        )

    @staticmethod
    def from_event(event: Dict) -> 'PieDataState':
        try:
            status: Dict = event['object']['status']
        except KeyError:
            return PieDataState(
                transition_time=None,
                message=None,
                reason='Event does not have .status field',
                type_=PieDataStateType.ConditionUnknown,
            )

        return PieDataState.from_status(status)


def get_event_sub(event) -> str:
    try:
        if not isinstance(event, dict):
            raise TypeError('Event must be a dictionary')

        sub = event['object']['metadata']['labels']['token.kubepie.io/sub']

        return sub
    except KeyError as e:
        raise ValueError(
            'Event\'s labels do not contain token.kubepie.io/sub') from e


def parse_event(event) -> Tuple[str, Optional[PieDataState]]:
    sub = get_event_sub(event)

    try:
        resource_state = PieDataState.from_event(event)
    except ValueError:
        return sub, None

    return sub, resource_state


def update_piedata_status(
        status_cache: Dict[str, Optional[PieDataState]], event):
    event_type = event['type']

    # logging.info('type=%s', event_type)

    if event_type == 'ADDED' or event_type == 'MODIFIED':
        try:
            sub, state = parse_event(event)
            status_cache[sub] = state

            logger.info(
                'Event type: %s, sub: %s, state: %s',
                event_type,
                sub,
                state)
        except ValueError as e:
            logger.error('Can not parse event: %s', e)
    elif event_type == 'DELETED':
        try:
            sub = get_event_sub(event)

            logger.info('Event type: %s, sub: %s', event_type, sub)

            try:
                del status_cache[sub]
            except KeyError:
                # Already deleted.
                pass
        except ValueError as e:
            logger.error('Can not parse event: %s', e)
