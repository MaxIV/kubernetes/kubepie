import copy
import logging
import secrets
import urllib.parse as url_parse
from datetime import datetime
from hashlib import sha1
from typing import Any, Dict, Optional, Union

from kubepie.common import kube_state, piecrust
from kubepie.piecut.lib.oidc_user_info import OIDCUserInfo
from kubepie.piecut.lib.user_url import compute_user_url

__CONST_HASH_SUFFIX_LEN = 8
# Maximum length for string values used in Kubernetes.
# Set to prevent from using too long strings.
__CONST_K8S_STR_VAL_MAXLEN = 2000

__CONST_K8S_MAX_NAME_LEN = 40
__CONST_K8S_MAX_LABEL_VALUE_LEN = 63


__PIEDATA_K8S_API_VERSION = (
    piecrust.PIEDATA_K8S_GROUP + "/" + piecrust.PIEDATA_K8S_VERSION
)


__PIEDATA_TOKEN_DOMAIN = "token.{}".format(piecrust.KUBEPIE_DOMAIN)
__PIEDATA_LABEL_SUB = "{}/sub".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_LABEL_ISSUER = "{}/issuer".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_LABEL_INSTANCE = piecrust.PIESEC_INSTANCE_LABEL
__PIEDATA_ANNOTATION_AUD = "{}/audience".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_EMAIL = "{}/email".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_FAMILY_NAME = "{}/family-name".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_GIVEN_NAME = "{}/given-name".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_NAME = "{}/name".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_ISSUER = "{}/issuer".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_PREFERRED_USERNAME = "{}/preferred-username".format(
    __PIEDATA_TOKEN_DOMAIN
)
__PIEDATA_ANNOTATION_PREFERRED_USERNAME_SANITIZED = (
    "{}/sanitized-preferred-username".format(__PIEDATA_TOKEN_DOMAIN)
)
__PIEDATA_ANNOTATION_SUB = "{}/sub".format(__PIEDATA_TOKEN_DOMAIN)
__PIEDATA_ANNOTATION_CLAIMS = piecrust.PIESEC_CLAIMS_ANNOTATION
__PIEDATA_ANNOTATION_REVISION = "{}/revision".format(piecrust.KUBEPIE_DOMAIN)

logger = logging.getLogger("KubePie.PieCut.lib.piedata")


def __sanitize_k8s(v: str, maxlen: int) -> str:
    """
    Cuts and sanitizes the given string to allow it to be used in Kubernetes
    .metadata.name, .metadata.namespace and .metadata.labels values.
    """
    if len(v) > __CONST_K8S_STR_VAL_MAXLEN:
        raise ValueError(
            "Given string is longer than {}".format(__CONST_K8S_STR_VAL_MAXLEN)
        )

    # Sanitize characters.
    new_str = list()

    for ch in v:
        nch = ch.lower()

        if nch.isalnum():
            new_str.append(nch)
            continue

        if nch == "@":
            new_str.append("-at-")
            continue

        new_str.append("-")

    # Cut trailing '-'
    if new_str[-1] == "-":
        new_str = new_str[:-1]

    # Check the string length and act accordingly.
    s_len = len(new_str)

    if maxlen <= __CONST_HASH_SUFFIX_LEN:
        # Use initial string to compute hash.
        v_hash = sha1(v.encode("utf-8"), usedforsecurity=False)
        cut_str = v_hash.hexdigest()[:__CONST_HASH_SUFFIX_LEN]

    elif s_len > maxlen:
        # Use initial string to compute hash.
        v_hash = sha1(v.encode("utf-8"), usedforsecurity=False)

        max_ind = maxlen - __CONST_HASH_SUFFIX_LEN - 1

        # Cut trailing '-'.
        if new_str[max_ind] == "-":
            max_ind = max_ind - 1

        new_str = new_str[:max_ind]

        new_str.append("-")
        new_str.append(v_hash.hexdigest()[:__CONST_HASH_SUFFIX_LEN])

        cut_str = "".join(new_str)

    else:
        cut_str = "".join(new_str)

    return cut_str


def __sanitize_k8s_label_value(v: str) -> str:
    """
    Replaces characters which are not supported
    in Kubernetes resource labels, names with '-'.
    Cuts the length to __CONST_K8S_MAX_LABEL_VALUE_LEN characters.
    """
    return __sanitize_k8s(v, __CONST_K8S_MAX_LABEL_VALUE_LEN)


def __merge(dest, src, path=None):
    """
    Merges src into dest.
    Values from src overwrite the values from dest, if the same key.
    """
    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("__merge: source: %s, dest: %s, path: %s", src, dest, path)

    if path is None:
        path = []
    for key in src:
        if key in dest:
            if isinstance(dest[key], dict) and isinstance(src[key], dict):
                __merge(dest[key], src[key], path + [str(key)])
            elif dest[key] == src[key]:
                pass  # same leaf value
            else:
                if logger.isEnabledFor(logging.DEBUG):
                    logger.debug(
                        "__merge: Overwrite key: %s, source value: %s", key, src[key]
                    )
                dest[key] = src[key]
        else:
            dest[key] = src[key]

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(
            "__merge: completed: source: %s, dest: %s, path: %s", src, dest, path
        )

    return dest


def compute_piedata_name(user_info: OIDCUserInfo) -> str:
    """
    Replaces characters which are not supported
    in Kubernetes resource labels, names with '-'.
    Cuts the length to __CONST_K8S_MAX_NAME_LEN
    characters as in some cases the name is used
    as a part of a longer resource name.
    """
    email_part = __sanitize_k8s(
        user_info.email, __CONST_K8S_MAX_NAME_LEN - (__CONST_HASH_SUFFIX_LEN + 1)
    )

    # Compute hash from "sub" as it is supposed to be unique.
    sub_part = sha1(user_info.sub.encode("utf-8"), usedforsecurity=False).hexdigest()

    return "-".join([email_part, sub_part[:__CONST_HASH_SUFFIX_LEN]])


def create_piedata_for_user(
    namespace: str,
    instance_name: str,
    external_url: Union[url_parse.ParseResult, url_parse.ParseResultBytes],
    user_info: OIDCUserInfo,
    original_url: Optional[str],
    extra_values: Dict[str, Any],
    revision: int,
    restart_enabled: bool,
    termination_enabled: bool,
    include_claims_json: bool,
    hostname_claim: Optional[str],
) -> Dict[str, Any]:
    if revision <= 0:
        raise ValueError(
            "__PIEDATA_ANNOTATION_REVISION must "
            "be positive, given value is {}".format(revision)
        )

    new_user_url = compute_user_url(user_info, external_url, original_url)

    # Merge extra values and the generated values.
    spec = copy.deepcopy(extra_values)

    spec = __merge(
        spec,
        {
            "extraLabels": {
                __PIEDATA_LABEL_SUB: (__sanitize_k8s_label_value(user_info.sub)),
                __PIEDATA_LABEL_ISSUER: (__sanitize_k8s_label_value(user_info.issuer)),
                # Mark PieData with PieSec instance.
                __PIEDATA_LABEL_INSTANCE: instance_name,
                __PIEDATA_ANNOTATION_PREFERRED_USERNAME_SANITIZED: (
                    __sanitize_k8s_label_value(user_info.preferred_username)
                ),
            },
            "extraAnnotations": {
                # Just for documentation purposes.
                __PIEDATA_ANNOTATION_AUD: user_info.audience,
                __PIEDATA_ANNOTATION_EMAIL: user_info.email,
                __PIEDATA_ANNOTATION_FAMILY_NAME: user_info.family_name,
                __PIEDATA_ANNOTATION_GIVEN_NAME: user_info.given_name,
                __PIEDATA_ANNOTATION_NAME: user_info.name,
                __PIEDATA_ANNOTATION_ISSUER: user_info.issuer,
                __PIEDATA_ANNOTATION_PREFERRED_USERNAME: (user_info.preferred_username),
                __PIEDATA_ANNOTATION_SUB: user_info.sub,
                __PIEDATA_ANNOTATION_CLAIMS: user_info.token_claims_json,
                __PIEDATA_ANNOTATION_REVISION: str(revision),
            },
            "authentication": {
                "remoteUserClaim": "preferred_username",
                "webDAV": {
                    "username": user_info.preferred_username
                    + "-"
                    + secrets.token_hex(16),
                    # Password is set in helm release in order to have it
                    # fixed between PieData restarts.
                    # 32 bytes is just 32 bytes and have no other
                    # meaning.
                    # I assume that 32 random bytes is more than enough
                    # to prevent brute-force attacks.
                    "password": secrets.token_hex(32),
                },
            },
            "authorization": {
                # Must be the value of the OIDC token "sub" field (Subject).
                "allowedUser": {
                    "claims": {
                        "sub": user_info.sub,
                        "email": user_info.email,
                    },
                },
            },
            "pieCutFlags": {
                "pieDataRestartEnabled": restart_enabled,
                "pieDataTerminationEnabled": termination_enabled,
                "pieDataRevision": str(revision),
            },
            "ingress": {
                "enabled": True,
                "hosts": [
                    {
                        "host": new_user_url.hostname,
                        "paths": [
                            {
                                "path": new_user_url.path,
                                "pathType": "Prefix",
                            },
                        ],
                    },
                ],
            },
        },
    )

    annotations: Dict[str, Any] = {
        __PIEDATA_ANNOTATION_REVISION: str(revision),
    }

    if include_claims_json:
        spec["tokenClaimsJSON"] = user_info.token_claims_json

        annotations.update(
            {
                __PIEDATA_ANNOTATION_AUD: user_info.audience,
                __PIEDATA_ANNOTATION_EMAIL: user_info.email,
                __PIEDATA_ANNOTATION_FAMILY_NAME: user_info.family_name,
                __PIEDATA_ANNOTATION_GIVEN_NAME: user_info.given_name,
                __PIEDATA_ANNOTATION_NAME: user_info.name,
                __PIEDATA_ANNOTATION_ISSUER: user_info.issuer,
                __PIEDATA_ANNOTATION_PREFERRED_USERNAME: (user_info.preferred_username),
                __PIEDATA_ANNOTATION_SUB: user_info.sub,
                __PIEDATA_ANNOTATION_CLAIMS: user_info.token_claims_json,
                __PIEDATA_ANNOTATION_REVISION: str(revision),
            }
        )

    if hostname_claim:
        try:
            hostname = __sanitize_k8s_label_value(getattr(user_info, hostname_claim)) + "-piedata"
            spec["hostnameOverride"] = hostname
        except AttributeError:
            pass

    res: Dict[str, Any] = {
        "apiVersion": __PIEDATA_K8S_API_VERSION,
        "kind": piecrust.PIEDATA_K8S_KIND,
        "metadata": {
            "name": compute_piedata_name(user_info),
            "namespace": namespace,
            "labels": {
                __PIEDATA_LABEL_SUB: (__sanitize_k8s_label_value(user_info.sub)),
                __PIEDATA_LABEL_ISSUER: (__sanitize_k8s_label_value(user_info.issuer)),
                # Mark PieData with PieSec instance just for
                # documentation purpose.
                __PIEDATA_LABEL_INSTANCE: instance_name,
            },
            "annotations": annotations,
        },
        "spec": spec,
    }

    return res


def create_json_patch_piedata_restart(
    existing_piedata: Dict[str, Any]
) -> Dict[str, Any]:
    """
    Modifies in place the given PieData to trigger its redeployment.
    """
    try:
        revision = existing_piedata["metadata"]["annotations"][
            __PIEDATA_ANNOTATION_REVISION
        ]  # noqa: E501
        revision = int(revision)
    except KeyError as e:
        raise KeyError(
            "PieData does not contain .metadata.annotations.{}".format(
                __PIEDATA_ANNOTATION_REVISION
            )
        ) from e
    except ValueError as e:
        raise ValueError(
            "PieData .metadata.annotations.{} must be a number".format(
                __PIEDATA_ANNOTATION_REVISION
            )
        ) from e

    revision += 1
    revision = str(revision)

    # ToDo: rewrite to use JSONPatch when the issue:
    # https://github.com/kubernetes-client/python/issues/2039
    # is closed.
    # Example requests:
    #  curl -H 'Content-Type: application/json-patch+json'  \
    #   -X PATCH localhost:8001/apis/kubepie.io/v1alpha1/namespaces/kubepie-test/piedata/dmitrii-ermakov-at-maxiv-lu-se-77b5b9d5/status \ # noqa: E501
    #   --data '[{"op": "replace", "path": "/metadata/annotations/annotat", "value": "value1"}, \ # noqa: E501
    #       {"op": "add", "path": "/status/conditions/-", "value": {"lastTransitionTime": "2024-09-01T08:57:00Z", "status": "True", "type": "Isssnitialized"} }]' # noqa: E501

    patch = existing_piedata
    patch["metadata"]["annotations"][__PIEDATA_ANNOTATION_REVISION] = revision
    patch["spec"]["extraAnnotations"][__PIEDATA_ANNOTATION_REVISION] = revision
    patch["spec"]["pieCutFlags"]["pieDataRevision"] = revision

    # patch = [
    #     {"op": "replace", "path": "/metadata/annotations/{}".format(__PIEDATA_ANNOTATION_REVISION), "value": revision},  # noqa: E501
    #     {"op": "replace", "path": "/spec/extraAnnotations/{}".format(__PIEDATA_ANNOTATION_REVISION), "value": revision},  # noqa: E501
    # ]

    return patch


def create_patch_piedata_restart_status(
    existing_piedata: Dict[str, Any]
) -> Dict[str, Any]:
    """
    Appends a new .status.conditions condition to PieData
    to mark the that the redeployment was triggered.
    """
    try:
        status = existing_piedata["status"]
    except KeyError:
        status = dict()

    try:
        conditions = existing_piedata["status"]["conditions"]
    except KeyError:
        conditions = list()
        status["conditions"] = conditions

    conditions.append(
        kube_state.PieDataState(
            transition_time=datetime.now(),
            message="PieData restart is triggered by user",
            reason=None,
            type_=kube_state.PieDataStateType.ConditionInitialized,
        ).dict()
    )

    return status
