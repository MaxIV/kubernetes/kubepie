{{/*
Expand the name of the chart.
*/}}
{{- define "piedata.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "piedata.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "piedata.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "piedata.labels" -}}
helm.sh/chart: {{ include "piedata.chart" . }}
{{ include "piedata.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "piedata.selectorLabels" -}}
app.kubernetes.io/name: {{ include "piedata.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- with (coalesce .Values.global.extraLabels .Values.extraLabels) }}
{{ toYaml . }}
{{- end }}
{{- end }}

{{/*
Common annotations
*/}}
{{- define "piedata.annotations" -}}
{{- with (coalesce .Values.global.extraAnnotations .Values.extraAnnotations) }}
{{ toYaml . }}
{{- end }}
# Authorized user claims:
  {{- if .Values.authorization.audiences }}
    {{- range $claim, $value := .Values.authorization.allowedUser.claims }}
      {{- if ne $claim "aud" }}
piesec.kubepie.io/{{ $claim | lower | replace "_" "-" }}: {{ $value | quote }}
      {{- end }}
    {{- end }}
piesec.kubepie.io/aud: {{ $.Values.authorization.audiences | join "," }}
  {{- else }}
    {{- range $claim, $value := .Values.authorization.allowedUser.claims }}
piesec.kubepie.io/{{ $claim | lower | replace "_" "-" }}: {{ $value | quote }}
    {{- end }}
  {{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "piedata.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "piedata.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Apache access control
*/}}
{{- define "piedata.apacheWebDAVAccessControl" }}
# Try to detect OAuth2 Authorization: Bearer XXX header
# and force OAuth2 authentication if present.
<If "%{HTTP:Authorization} =~ m#^Bearer#i">
  AuthType oauth2

  {{- $_ := required  ".Values.authorization.allowedUser.claims must NOT be empty" .Values.authorization.allowedUser.claims }} {{/* A primitive validation */}}
  {{- if .Values.authorization.audiences }}
  # Apache authorization.
  <RequireAll>
    <RequireAll>
    {{- range $claim, $value := $.Values.authorization.allowedUser.claims }}
      {{- if ne $claim "aud"}}{{/* Ignore "aud" claim if present as it is overridden by "audiences" */}}
      Require oauth2_claim '{{ $claim }}:{{ $value }}'
      {{- end }}
    {{- end }}
    </RequireAll>

    # Any of the audiences will be allowed.
    <RequireAny>
    {{- range $aud := .Values.authorization.audiences }}
      Require oauth2_claim 'aud:{{ $aud }}'
    {{- end }}
    </RequireAny>
  </RequireAll>
  {{- else }}
  <RequireAll>
    {{- range $claim, $value := .Values.authorization.allowedUser.claims }}
    Require oauth2_claim "{{ $claim }}:{{ required (printf "Value of .Values.authorization.allowedUser.claims.%s must NOT be empty" $claim ) $value }}"
    {{- end }}
  </RequireAll>
  {{- end }}
</If>
<Else>
  AuthType Basic
  AuthName "PieData WebDAV"
  AuthUserFile "/var/run/apache2/htpasswd"
  AuthBasicAuthoritative On

  <RequireAll>
    Require valid-user
  </RequireAll>
</Else>
{{- end }}

{{- define "piedata.apacheAccessControl" }}
# Try to detect OAuth2 Authorization: Bearer XXX header
# and force OAuth2 authentication if present.
<If "%{HTTP:Authorization} =~ m#^Bearer#i">
  AuthType oauth2

  {{- $_ := required  ".Values.authorization.allowedUser.claims must NOT be empty" .Values.authorization.allowedUser.claims }} {{/* A primitive validation */}}
  {{- if .Values.authorization.audiences }}
  # Apache authorization.
  <RequireAll>
    <RequireAll>
    {{- range $claim, $value := $.Values.authorization.allowedUser.claims }}
      {{- if ne $claim "aud"}}{{/* Ignore "aud" claim if present as it is overridden by "audiences" */}}
      Require oauth2_claim '{{ $claim }}:{{ $value }}'
      {{- end }}
    {{- end }}
    </RequireAll>

    # Any of the audiences will be allowed.
    <RequireAny>
    {{- range $aud := .Values.authorization.audiences }}
      Require oauth2_claim 'aud:{{ $aud }}'
    {{- end }}
    </RequireAny>
  </RequireAll>
  {{- else }}
  <RequireAll>
    {{- range $claim, $value := .Values.authorization.allowedUser.claims }}
    Require oauth2_claim "{{ $claim }}:{{ required (printf "Value of .Values.authorization.allowedUser.claims.%s must NOT be empty" $claim ) $value }}"
    {{- end }}
  </RequireAll>
  {{- end }}
</If>
<Else>
  AuthType openid-connect

  {{- $_ := required  ".Values.authorization.allowedUser.claims must NOT be empty" .Values.authorization.allowedUser.claims }} {{/* A primitive validation */}}
  {{- if .Values.authorization.audiences }}
  # Apache authorization.
  <RequireAll>
    # Any of the audiences will be allowed.
    <RequireAny>
    {{- range $aud := .Values.authorization.audiences }}
      Require claim "aud:{{ $aud }}"
    {{- end }}
    </RequireAny>

    # All other claims must be satisfied.
    {{- range $claim, $value := $.Values.authorization.allowedUser.claims }}
      {{- if ne $claim "aud"}}{{/* Ignore "aud" claim if present as it is overridden by "audiences" */}}
    Require claim "{{ $claim }}:{{ required (printf "Value of .Values.authorization.allowedUser.claims.%s must NOT be empty" $claim ) $value }}"
      {{- end }}
    {{- end }}
  </RequireAll>
  {{- else }}
  <RequireAll>
    {{- range $claim, $value := .Values.authorization.allowedUser.claims }}
    Require claim "{{ $claim }}:{{ required (printf "Value of .Values.authorization.allowedUser.claims.%s must NOT be empty" $claim ) $value }}"
    {{- end }}
  </RequireAll>
  {{- end }}
</Else>
{{- end }}

{{/*
Construct the image to use
*/}}
{{- define "piedata.image" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.image.tag .Values.global.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.image.repository $tag -}}
{{- end -}}
{{- end -}}

{{/*
Construct the image to use
*/}}
{{- define "piedata.pieTrackImage" -}}
{{- $tag :=  default .Chart.AppVersion (coalesce .Values.global.image.tag .Values.monitoring.image.tag) | toString -}}
{{- $registry := coalesce .Values.global.image.registry .Values.monitoring.image.registry }}
{{- if $registry }}
{{- printf "%s/%s:%s" $registry .Values.monitoring.image.repository $tag -}}
{{- else -}}
{{- printf "%s:%s" .Values.monitoring.image.repository $tag -}}
{{- end -}}
{{- end -}}

{{- define "piedata.firstIngressPathPrefix" -}}
{{- if .Values.ingress.enabled -}}
/{{- (first ( first .Values.ingress.hosts ).paths ).path | trimAll "/" -}}
{{- else -}}/{{- end -}}
{{- end -}}

{{- define "piedata.strToBool" -}}
  {{- $output := "" -}}
  {{- $inputS := (. | toString) -}}
  {{- if or (eq $inputS "true") (eq $inputS "yes") (eq $inputS "on") -}}
      {{- $output = "1" -}}
  {{- end -}}
  {{ $output }}
{{- end -}}

{{- define "piedata.extraServicePort" -}}
{{- add 2500 . }}
{{- end }}

{{- define "piedata.extraServiceMonitorPort" -}}
{{- add 3000 . }}
{{- end }}

{{/*
Port name must be at most 15 characters long
*/}}
{{- define "piedata.extraServicePortName" }}
{{- $portName := . }}
  {{- if ge (len $portName) 13 }}
    {{- regexReplaceAll "(\\W|_)+" (printf "i%s-%s" ($portName | trunc 7) ($portName | sha1sum | trunc 6)) "-" | trimAll "-" | lower }}
  {{- else }}
    {{- regexReplaceAll "(\\W|_)+" (printf "i%s" $portName) "-" | lower }}
  {{- end }}
{{- end }}

{{- define "piedata.extraServiceMonitorPortName" }}
{{- $portName := . }}
  {{- if ge (len $portName) 13 }}
    {{- regexReplaceAll "(\\W|_)+" (printf "m%s-%s" ($portName | trunc 7) ($portName | sha1sum | trunc 6)) "-" | lower }}
  {{- else }}
    {{- regexReplaceAll "(\\W|_)+" (printf "m%s" $portName) "-" | lower }}
  {{- end }}
{{- end }}

{{/*
Port name must be at most 15 characters long
Port name for Apache on which it listens for proxy requests
*/}}
{{- define "piedata.extraServiceProxyPortName" }}
{{- $portName := . }}
  {{- if ge (len $portName) 13 }}
    {{- regexReplaceAll "(\\W|_)+" (printf "p%s-%s" ($portName | trunc 7) ($portName | sha1sum | trunc 6)) "-" | lower }}
  {{- else }}
    {{- regexReplaceAll "(\\W|_)+" (printf "p%s" $portName) "-" | lower }}
  {{- end }}
{{- end }}

{{/*
Container name must be at most 15 characters long
*/}}
{{- define "piedata.extraServiceName" }}
{{- $portName := . }}
  {{- if ge (len $portName) 13 }}
    {{- regexReplaceAll "(\\W|_)+" (printf "c%s-%s" ($portName | trunc 7) ($portName | sha1sum | trunc 6)) "-" | lower }}
  {{- else }}
    {{- regexReplaceAll "(\\W|_)+" (printf "c%s" $portName) "-" | lower }}
  {{- end }}
{{- end }}

{{/*
Paths should be lowercase and urlencoded
*/}}
{{- define "piedata.extraServiceIngressPathSuffix" }}
  {{- regexReplaceAll "(\\W|_)+" . "-" | trimAll "-" | lower }}
{{- end }}

{{/*
Apache ServerName must be one word.
*/}}
{{- define "piedata.extraServiceServerName" }}
  {{- regexReplaceAll "(\\W|_)+" . "-" | trimAll "-" | lower }}
{{- end }}

{{/*
Helper to generate correct base URL path for extra containers
*/}}
{{- define "piedata.extraServiceBasePath" }}
  {{- include "piedata.firstIngressPathPrefix" . }}/{{ include "piedata.extraServiceIngressPathSuffix" .WorkloadName }}
{{- end }}