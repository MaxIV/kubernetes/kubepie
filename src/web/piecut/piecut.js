// display error alert
function showAlert(header, message) {
    const alertContainer = document.getElementById("alertContainer");
    alertContainer.style.display = "block";

    const alertDiv = document.createElement("div");
    alertDiv.className = "alert alert-danger alert-dismissible fade show";
    alertDiv.role = "alert";
    alertDiv.innerHTML = `
      <h4 class="alert-heading">${header}</h4>
      <p>${message}</p>
      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  `;

    alertContainer.appendChild(alertDiv);
}

// cycle over KubePie colors
function cycleSpinnerColors() {
    const spinner = document.getElementById("dynamicSpinner");
    const colors = ["red", "orange", "yellow", "green", "light-blue", "blue", "purple"]; // KubePie colors
    let color_index = 0;
    setInterval(() => {
        spinner.className = `spinner-grow kubepie-color-${colors[color_index]}`;
        color_index = (color_index + 1) % colors.length;
    }, 3000);
}

// display baking status
function setStatusMessage(text) {
    document.getElementById("pieCutMessage").innerHTML = text;
}

// query the baking status
async function queryBakingStatus(maxRetryCount = 3) {
    const PieDataFailureHTTPCode = 500;
    // get-status URL
    const searchParams = new URLSearchParams(window.location.search);
    const url = new URL(window.location.origin + "/kubepie/piecut/get-status?" + searchParams.toString());
    // attempt several times
    for (let attempt = 1; attempt <= maxRetryCount; attempt++) {
        let response = null;
        try {
            response = await fetch(url);
            if (!response.ok) {
                throw new Error(`Fetching PieData baking status failed: HTTP status ${response.status}`);
            }
            const data = await response.json();
            console.log("PieData Status:", data);
            return data;
        } catch (error) {
            console.error(`Attempt ${attempt} failed:`, error);
            if (attempt === maxRetryCount) {
                console.error("Max retry count reached. Giving up.");
                error_message = "Try to refresh the page. If error persists, please contact the support.";
                if (response && response.status == PieDataFailureHTTPCode) {
                    const error_body = await response.text();
                    showAlert("Internal error in PieData baking", error_body + "<br/>" + error_message);
                } else {
                    showAlert("PieData baking was not successfull", error_message);
                }
                // hide spinner on error
                const spinner = document.getElementById("dynamicSpinner");
                if (spinner) {
                    spinner.style.visibility = "hidden";
                }
                return null;
            }
        }
    }
}

// randomize user greeting
function getRandomGreeting() {
    const greetings = [
        "Hi", "Hello", "Hey", "Hey there", "Howdy", "Good day",
        "Good to see you", "Nice to see you", "Hope you are doing well",
        "Greetings", "Pleasure to meet you"
    ];
    return greetings[Math.floor(Math.random() * greetings.length)];
}

// get message to show to user
function getProgressMessage() {
    let pieCutMessage = document.getElementById('piecut-message').content;
    if (pieCutMessage == null) {
        pieCutMessage = "We are baking PieData for you!";
    }
    return pieCutMessage;
}

function checkInterval() {
    const checkIntervalSec = 3;
    return checkIntervalSec * 1000;
}

// baking wait-loop
async function waitPieData() {
    const pieCutGreeting = getRandomGreeting();
    const username_attribute = document.getElementById('piecut-username-attribute').content;

    while (true) {
        const status = await queryBakingStatus();
        let message = getProgressMessage();
        if (!status) {
            setStatusMessage("KubePie encounter a problem baking your PieData");
            throw new Error("Failed to get PieData status");
        }

        let username;
        try {
            if (username_attribute && username_attribute in status["user"]) {
                username = status["user"][username_attribute];
            } else {
                username = status["user"]["preferred_username"];
            }
        } catch (error) {
            username = "Valued Guest";
        }

        setStatusMessage(`${pieCutGreeting}, ${username}!<br/>${message}!`)
        if (status["is_ready"] == true) {
            let redirect_to = status["redirect_to"];
            if (redirect_to == null) {
                redirect_to = "/";
            }
            window.location.pathname = redirect_to;
        }
        await new Promise(resolve => setTimeout(resolve, checkInterval()));
    }
}

// start wait-loop
document.addEventListener("DOMContentLoaded", () => {
    cycleSpinnerColors();
    waitPieData();
});
