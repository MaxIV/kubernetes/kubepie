import base64
import logging
import json
from typing import Optional, Dict, Any

import yarl
from aiohttp import web, web_exceptions
from kubepie.common import kube_state
from kubepie.piecut.lib import piedata
from kubepie.piecut.lib.oidc_user_info import OIDCUserInfo
from kubepie.piecut.web.external_url import external_url_get
from kubepie.piecut.web.user_info import user_info_set, user_status_get

# Key in aiohttp Application to store parsed original path.
CONST_PIECUT_ORIG_PATH_KEY = "piecut_orig_path"
CONST_ORIG_PATH_QUERY_PARAM = "orig_path"

logger = logging.getLogger("KubePie.PieCut.web.middleware")


def __extract_token_json(token: str) -> str:
    claims_raw = token.split(".")[1]

    padding = len(claims_raw) % 4
    if padding == 2:
        claims_raw += "=="
    elif padding == 3:
        claims_raw += "="

    json_str = base64.urlsafe_b64decode(claims_raw).decode("utf-8")

    return json_str


def create_check_authentication_mw(debug_token: str):
    @web.middleware
    async def check_oidc_authentication(request: web.Request, handler):
        """
        The authentication assumes that the application runs
        behind a reverse proxy which sets special headers.
        The proxy and the headers are assumed to be the source of truth.

        Current implementation uses headers set by apache mod auth oidc:
            https://github.com/zmartzone/mod_auth_openidc/ but
        any other proxy which can set user information should work.
        """
        # Use a static token for health probes and debug.
        if request.path.startswith("/kubepie/piecut/privileged"):
            if request.headers.get("DEBUG-TOKEN") == debug_token:
                return await handler(request)
            else:
                return web.HTTPUnauthorized(
                    text="DEBUG-TOKEN header is not provided or incorrect."
                )

        # Debug headers.
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug("request headers: %s", request.headers)

        # Decide which authentication method is used.
        # If it is OAuth2, then Authorization: Bearer XXX token
        # will present.
        authz_header = request.headers.get("Authorization")

        if authz_header is not None:
            authz_header_value = authz_header.split()

            # Only if the Authorization header contains a valid Bearer XXX
            # the authentication is allowed.
            if (
                len(authz_header_value) == 2
                and authz_header_value[0].lower() == "bearer"
            ):
                # Try to extract authentication headers
                # provided by Apache OAuth2 module.
                token_claims_raw = __extract_token_json(authz_header_value[1])
            else:
                raise web_exceptions.HTTPUnauthorized(
                    reason="Provided Authorization header "
                    "is not a valid OAuth2 Bearer token"
                )
        # No Authorization: Bearer XXX header => do normal OpenID flow.
        else:
            # Try to extract authentication headers
            # provided by Apache OIDC module.
            oidc_access_token = request.headers["OIDC_Access_Token"]

            if oidc_access_token is None or len(oidc_access_token) == 0:
                raise web_exceptions.HTTPUnauthorized(
                    reason="OpenID Connect Token is not provided"
                )

            token_claims_raw = __extract_token_json(oidc_access_token)

        token_claims: Dict[str, Any] = json.loads(token_claims_raw)

        try:
            # Strongly required token claims.
            audience = token_claims["aud"]
            email = token_claims["email"]
            issuer = token_claims["iss"]
            sub = token_claims["sub"]
        except KeyError as e:
            raise web_exceptions.HTTPInternalServerError(
                reason="Incorrect authentication data provided"
            ) from e

        # Optional headers - set to an empty string as
        # default/fallback.
        family_name = token_claims.get("family_name", "")
        given_name = token_claims.get("given_name", "")
        name = token_claims.get("name", "")
        preferred_username = token_claims.get("preferred_username", "")

        user_info_set(
            request,
            OIDCUserInfo(
                # access_token=access_token,
                audience=audience,
                email=email,
                family_name=family_name,
                given_name=given_name,
                name=name,
                issuer=issuer,
                preferred_username=preferred_username,
                sub=sub,
                token_claims_json=token_claims_raw,
            ),
        )

        return await handler(request)

    return check_oidc_authentication


@web.middleware
async def redirect_to_active_piedata(request: web.Request, handler):
    # Encode original URL path if not set.
    orig_path = _orig_path_decode(request)
    if orig_path is None:
        orig_path = request.url.path
        _orig_path_set(request, orig_path)

    # Check status and redirect if ready.
    user_status, user_info = user_status_get(request)

    if (
        user_status is not None
        and user_info is not None
        and user_status.type_ == kube_state.PieDataStateType.ConditionDeployed
    ):
        # Shortcut redirect to active PieData.
        return await __redirect_to_deployed(request, user_info, orig_path)

    return await handler(request)


@web.middleware
async def save_original_path(request: web.Request, handler):
    # Encode original URL path if not set.
    orig_path = _orig_path_decode(request)
    if orig_path is None:
        orig_path = request.url.path

    _orig_path_set(request, orig_path)

    return await handler(request)


async def redirect_to_deployed(
    request: web.Request, user_info: OIDCUserInfo
) -> web.Response:
    orig_path = _orig_path_get(request)

    return await __redirect_to_deployed(request, user_info, orig_path)


def inject_orig_path(url: yarl.URL, request: web.Request) -> yarl.URL:
    orig_path = _orig_path_get(request)

    if orig_path is None:
        return url

    qs = dict(url.query)

    qs[CONST_ORIG_PATH_QUERY_PARAM] = orig_path

    return url.with_query(qs)


def _orig_path_get(request: web.Request) -> Optional[str]:
    return request.get(CONST_PIECUT_ORIG_PATH_KEY)


def _orig_path_set(request: web.Request, orig_path: str):
    request[CONST_PIECUT_ORIG_PATH_KEY] = orig_path


def _orig_path_decode(request: web.Request) -> Optional[str]:
    return request.query.get(CONST_ORIG_PATH_QUERY_PARAM)


async def __redirect_to_deployed(
    request: web.Request, user_info: OIDCUserInfo, orig_path: Optional[str]
) -> web.Response:
    ext_url = external_url_get(request)

    new_url = piedata.compute_user_url(user_info, ext_url, orig_path)

    # TODO: 302 instead of 307 as a workaround for ARC.
    # Make it configurable in the chart?
    # return web.HTTPTemporaryRedirect(new_url.geturl())
    return web.HTTPFound(new_url.geturl())


def compute_redirect_url(request: web.Request, user_info: OIDCUserInfo) -> str:
    ext_url = external_url_get(request)
    orig_path = _orig_path_get(request)

    new_url = piedata.compute_user_url(user_info, ext_url, orig_path)

    return new_url.geturl()
