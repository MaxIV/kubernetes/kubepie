import logging
import ssl
from ldap3 import Server, Connection, Tls, ALL, ALL_ATTRIBUTES
from ldap3.core.exceptions import LDAPException
from .common import UserMap, PieSecConfigException

#
# Plugin configuration provided as following JSON:
#     {
#         "ldap": {
#             "server": "<ldap hostname>",
#             "basedn": "<ldap base DN>",
#             "binddn": "<ldap bind username>",
#             "bindpw": <ldap bind secret>"
#         },
#         "schema": {
#             "user_search_filter": "(&(objectClass=person)(uidNumber=*)(sAMAccountName={preferred_username}))",
#             "group_search_filter": "(&(objectClass=group)(gidNumber=*))"
#         }
#     }
#  NOTE! User search filter includes the .format substitutions from claims
#

class LDAPUserMap(UserMap):
    """ Implement User Data Fetching from LDAP """
    def __init__(self, configs=[]) -> None:
        default_config = """{
            "ldap": {},
            "schema": {
              "user_search_filter": "(&(objectClass=person)(uidNumber=*)(sAMAccountName={preferred_username}))",
              "group_search_filter": "(&(objectClass=group)(gidNumber=*))"
            }
        }"""
        self.logger = logging.getLogger('KubePie.PieSec.Plugin.LDAP')
        super().__init__(configs, defaults=default_config)
        # check mandatory configuration
        if 'server' not in self.config['ldap']:
            raise PieSecConfigException('LDAP server is required but not defined in the configuration')
        if 'basedn' not in self.config['ldap']:
            raise PieSecConfigException('LDAP Base DN is required but not defined in the configuration')
        if 'binddn' not in self.config['ldap']:
            raise PieSecConfigException('LDAP Bind DN is required but not defined in the configuration')
        if 'bindpw' not in self.config['ldap']:
            raise PieSecConfigException('LDAP Bind Password is required but not defined in the configuration')

    def __get_groups(self, conn, claims):
        """Get all LDAP groups"""
        data = {}
        entry_generator = conn.extend.standard.paged_search(
                search_base = self.config['ldap']['basedn'],
                search_filter = self.config['schema']['group_search_filter'].format(**claims),
                attributes=[
                    'gidNumber',
                    'memberOf'
                ],
                paged_size = 1000,
                generator=True
        )
        for e in entry_generator:
            if 'dn' in e:
                data[e['dn']] = e['attributes']
        return data

    def __get_user_sgids(self, groups, memberof, nlevel=1):
        """Recursively get nested groups"""
        sgids = set()
        for group_dn in memberof:
            if group_dn in groups:
                sgids.add(int(groups[group_dn]['gidNumber']))
                if nlevel > 5:
                    self.logger.warning('Reached the nesting limit on %s group', group_dn)
                    return sgids
                if 'memberOf' in groups[group_dn] and groups[group_dn]['memberOf']:
                    sgids.update(self.__get_user_sgids(groups, groups[group_dn]['memberOf'], nlevel+1))
        return sgids


    def map_user(self, claims):
        map_data = {}
        # establish connectin to LDAP
        try:
            self.logger.info('Establishing connection to LDAP server %s', self.config['ldap']['server'])
            tls = Tls(validate = ssl.CERT_NONE, version=ssl.PROTOCOL_TLS_CLIENT, ciphers='DEFAULT:@SECLEVEL=1')
            server = Server(self.config['ldap']['server'], use_ssl=True, tls=tls, get_info=ALL)
            # TODO: 90 seconds timeout somewhere that is longer than webhook timeout
            conn = Connection(server, user=self.config['ldap']['binddn'],
                            password=self.config['ldap']['bindpw'], auto_bind=True)

            groups = self.__get_groups(conn, claims)
            entry_generator = conn.extend.standard.paged_search(
                    search_base = self.config['ldap']['basedn'],
                    search_filter = self.config['schema']['user_search_filter'].format(**claims),
                    attributes=[
                        'uidNumber',
                        'gidNumber',
                        'memberOf'
                    ],
                    paged_size = 1000,
                    generator=True
            )
            for e in entry_generator:
                if 'dn' in e:
                    map_data = e['attributes']
                    map_data['sgids'] = set()
                    if 'memberOf' in map_data and map_data['memberOf']:
                        map_data['sgids'].update(self.__get_user_sgids(groups, map_data['memberOf']))
                    break
        except LDAPException as e:
            self.logger.error('Filed to fetch information from LDAP. %s', str(e))
            return False
        except KeyError as e:
            self.logger.error('Failed to construct LDAP search filter. The %s key is missing in the provided claims.', str(e))
            return False
        if not map_data:
            self.logger.error('User matching the claims %s is not found in the LDAP', str(claims))
            return False
        # set class fields on success
        self.data['uid'] = int(map_data['uidNumber'])
        self.data['gid'] = int(map_data['gidNumber'])
        self.data['sgids'] = list(map_data['sgids'])
        self.logger.info('User mapped to: %s', str(map_data))
        return True

