import asyncio
import logging
import typing
from datetime import datetime
from random import randbytes
from typing import Any, Dict, Optional

from aiohttp import web
from kubepie.common import kube_state, piecrust
from kubepie.piecut.lib import piedata
from kubepie.piecut.lib.oidc_user_info import OIDCUserInfo
from kubepie.piecut.web.external_url import external_url_get
from kubepie.piecut.web.config import (
    instance_name_get,
    wait_loop_file_get,
    hostname_override_claim_get,
)
from kubepie.piecut.web.middleware import inject_orig_path
from kubepie.piecut.web.namespace import namespace_get
from kubepie.piecut.web.user_info import user_status_delete, user_status_set
from kubernetes_asyncio import client
from kubernetes_asyncio.client.api_client import ApiClient

# To store extra PieData values loaded from a config file.
CONST_PIECUT_PIEDATA_EXTRA_VALUES_KEY = "piecut_piedata_values"
# Time to wait before redirecting a CLI client to the next step of
# redirect loop.
__CONST_REDIRECT_DELAY_SEC = 10

logger = logging.getLogger("KubePie.PieCut.web.piedata")


def piedata_values_set(app: web.Application, vals: Dict[str, Any]):
    app[CONST_PIECUT_PIEDATA_EXTRA_VALUES_KEY] = vals


def piedata_values_get(request: web.Request) -> Dict[str, Any]:
    return request.app[CONST_PIECUT_PIEDATA_EXTRA_VALUES_KEY]


async def get_piedata(api, namespace: str, user_info: OIDCUserInfo) -> Dict[str, Any]:
    """
    Performs a lookup for an existing PieData CRD.
    """
    try:
        existing_piedata = await client.CustomObjectsApi(
            api
        ).get_namespaced_custom_object(
            piecrust.PIEDATA_K8S_GROUP,
            piecrust.PIEDATA_K8S_VERSION,
            namespace,
            piecrust.PIEDATA_K8S_KIND_PLURAL,
            piedata.compute_piedata_name(user_info),
        )

        logger.debug(
            "An existing PieData found, name: %s",
            existing_piedata["metadata"]["name"],
        )

        return existing_piedata
    except client.ApiException as e:
        if e.status == 404:
            raise web.HTTPNotFound(text="A running PieData is not found") from e
        else:
            raise web.HTTPInternalServerError(text="Get PieData failed") from e


async def create_piedata(
    request: web.Request,
    user_info: OIDCUserInfo,
    restart_enabled: bool,
    termination_enabled: bool,
    include_claims_json: bool,
) -> Dict[str, Any]:
    """
    Creates a new PieData or return existing one.
    """
    ext_url = external_url_get(request)
    namespace = namespace_get(request)
    instance_name = instance_name_get(request)
    hostname_claim = hostname_override_claim_get(request)

    async with ApiClient() as api:
        # Check that there is definitely no PieData for this user.
        # This is to prevent a race when the request
        # to create a new PieData
        # has already sent but the controller has not yet reacted to the
        # 'ADDED' event so the user_status is None
        # while the PieData is created.
        # Another cause of such a state is when
        # we have multiple controllers
        # which work as load-balanced processes
        # and they do not manage to react on time.
        # The drawback is that we will produce one extra Kube-API call
        # but it should be that under normal circumstances the call will
        # happen only on 'Initial access', i.e. when there is no
        # a PieData for a user.
        try:
            existing_piedata = await client.CustomObjectsApi(
                api
            ).get_namespaced_custom_object(
                piecrust.PIEDATA_K8S_GROUP,
                piecrust.PIEDATA_K8S_VERSION,
                namespace,
                piecrust.PIEDATA_K8S_KIND_PLURAL,
                piedata.compute_piedata_name(user_info),
            )
            logger.debug(
                "An existing PieData found, namespace=%s, name=%s",
                existing_piedata["metadata"]["namespace"],
                existing_piedata["metadata"]["name"],
            )

            return existing_piedata

        except client.ApiException as e:
            if e.status == 404:
                logger.debug(
                    "An existing PieData for %s is not found - creating",
                    user_info.email,
                )

        # Create a new PieData.
        extra_values: Dict[str, Any] = piedata_values_get(request)

        new_piedata = piedata.create_piedata_for_user(
            namespace=namespace,
            instance_name=instance_name,
            external_url=ext_url,
            user_info=user_info,
            original_url=request.query.get("request_url"),
            extra_values=extra_values,
            revision=1,
            restart_enabled=restart_enabled,
            termination_enabled=termination_enabled,
            include_claims_json=include_claims_json,
            hostname_claim=hostname_claim,
        )

        logger.debug("Creating a new PieData: %s", new_piedata["metadata"]["name"])

        try:
            user_status_set(
                request,
                user_info,
                kube_state.PieDataState(
                    transition_time=datetime.now(),
                    message="Created",
                    reason=None,
                    type_=kube_state.PieDataStateType.ConditionCreated,
                ),
            )

            created_piedata = await client.CustomObjectsApi(
                api
            ).create_namespaced_custom_object(
                piecrust.PIEDATA_K8S_GROUP,
                piecrust.PIEDATA_K8S_VERSION,
                namespace,
                piecrust.PIEDATA_K8S_KIND_PLURAL,
                new_piedata,
            )

        except client.ApiException as e:
            # Remove status from the user.
            user_status_delete(request, user_info)

            raise e

        logger.debug("Created a new PieData: %s", created_piedata["metadata"]["name"])

        return created_piedata


async def kick_piedata(
    request: web.Request, user_info: OIDCUserInfo, user_status: kube_state.PieDataState
) -> Dict[str, Any]:
    namespace = namespace_get(request)

    async with ApiClient() as api:
        # Check that there is definitely no PieData for this user.
        # This is to prevent a race when the request
        # to create a new PieData
        # has already sent but the controller has not yet reacted to the
        # 'ADDED' event so the user_status is None
        # while the PieData is created.
        # Another cause of such a state is when
        # we have multiple controllers
        # which work as load-balanced processes
        # and they do not manage to react on time.
        # The drawback is that we will produce one extra Kube-API call
        # but it should be that under normal circumstances the call will
        # happen only on 'Initial access', i.e. when there is no
        # a PieData for a user.
        existing_piedata = await get_piedata(api, namespace, user_info)

        try:
            patch = piedata.create_json_patch_piedata_restart(existing_piedata)
        except KeyError as e:
            raise web.HTTPInternalServerError(
                text="Incorrect PieData .metadata.annotations"
            ) from e
        except ValueError as e:
            raise web.HTTPInternalServerError(
                text="Incorrect PieData .metadata.annotations"
            ) from e

        # ToDo: rewrite to use JSONPatch when the issue:
        # https://github.com/kubernetes-client/python/issues/2039
        # is closed.
        updated = await client.CustomObjectsApi(api).patch_namespaced_custom_object(
            piecrust.PIEDATA_K8S_GROUP,
            piecrust.PIEDATA_K8S_VERSION,
            namespace,
            piecrust.PIEDATA_K8S_KIND_PLURAL,
            existing_piedata["metadata"]["name"],
            patch,
            _content_type="application/merge-patch+json",
        )

        logger.debug(
            "PieData updated: namespace=%s, name=%s",
            namespace,
            updated["metadata"]["name"],
        )

        return updated


async def kill_piedata(
    request: web.Request, user_info: OIDCUserInfo, user_status: kube_state.PieDataState
) -> Dict[str, Any]:
    namespace = namespace_get(request)

    async with ApiClient() as api:
        # Check that there is definitely no PieData for this user.
        # This is to prevent a race when the request
        # to create a new PieData
        # has already sent but the controller has not yet reacted to the
        # 'ADDED' event so the user_status is None
        # while the PieData is created.
        # Another cause of such a state is when
        # we have multiple controllers
        # which work as load-balanced processes
        # and they do not manage to react on time.
        # The drawback is that we will produce one extra Kube-API call
        # but it should be that under normal circumstances the call will
        # happen only on 'Initial access', i.e. when there is no
        # a PieData for a user.
        existing_piedata = await get_piedata(api, namespace, user_info)

        deleted = await client.CustomObjectsApi(api).delete_namespaced_custom_object(
            piecrust.PIEDATA_K8S_GROUP,
            piecrust.PIEDATA_K8S_VERSION,
            namespace,
            piecrust.PIEDATA_K8S_KIND_PLURAL,
            existing_piedata["metadata"]["name"],
        )

        logger.debug(
            "PieData deleted: namespace=%s, name=%s",
            namespace,
            deleted["metadata"]["name"],
        )

        return deleted


async def report_failure(
    logger: logging.Logger,
    request: web.Request,
    message: str,
    user_info: Optional[OIDCUserInfo],
    user_status: Optional[kube_state.PieDataState],
    exception: Optional[Exception],
    error_class: typing.Any = web.HTTPInternalServerError,
) -> web.Response:
    if user_info is None:
        email = "No user_info"
    else:
        email = user_info.email

    logger.error(
        "Can not deploy PieData for %s,message: %s, state: %s, exception: %s",
        email,
        message,
        user_status,
        exception,
    )
    return error_class(
        text=(
            "We could not bake a pie for you, "
            "please try again in a few minutes. "
            "If the problem persists, please contact a "
            "system administrator. "
            "Please, be ready to tell the administrator "
            "your email and time "
            "when the issue appeared."
        )
    )


async def redirect_to_wait(
    request: web.Request, user_info: OIDCUserInfo
) -> web.Response:
    # Distinguish between desktop and mobile browsers, i.e.
    # Firefox, Chromium etc.
    # and "other" clients, i.e. Python requests, curl etc.
    # The desktop browsers will be shown a "nice" web page with
    # info about current status while
    # other browsers will be sent to the redirect loop.
    user_agent = request.headers.get("User-Agent")

    logger.debug("User-Agent: %s", user_agent)

    # The variable progress_bar is set to true if a client browser
    # is supposed to support HTML and Javascript (best effort guess).
    # If progress_bar is true, then the redirect is sent as an HTTP/200
    # and a nice HTML + JS page which will show progress to a user.
    # Otherwise, HTTP/307 will be sent after a short delay, so that
    # HTTP clients such as curl will be able to 'wait in loop' until the
    # PieData is ready.
    if user_agent is None:
        html_wait_loop = False
    else:
        browser_vendor = user_agent.split("/")[0].lower()

        logger.debug("Browser vendor: %s", browser_vendor)

        if browser_vendor in {
            "chromium",
            "chrome",
            "firefox",
            "mozilla",
            "seamonkey",
            "safari",
            "opr",
            "opera",
        }:
            html_wait_loop = True
        else:
            html_wait_loop = False

    logger.debug("Is ui requested: %s", html_wait_loop)

    if html_wait_loop:
        # Show "please-wait" page.
        u = request.app.router["static-files"].url_for(
            filename=wait_loop_file_get(request)
        )
        u = inject_orig_path(u, request)

        logger.debug("Redirect to UI URL: %s", u)

        return web.HTTPTemporaryRedirect(u)
    else:
        # Wait and redirect-loop.
        await asyncio.sleep(__CONST_REDIRECT_DELAY_SEC)

        u = (
            request.app.router["wait-piedata"]
            .url_for(rand_string=randbytes(4).hex())
            .with_query(request.query)
        )

        u = inject_orig_path(u, request)

        # TODO: 302 instead of 307 as a workaround for ARC. Make it configurable in the chart? # noqa: E501
        # return web.HTTPTemporaryRedirect(u)
        return web.HTTPFound(u)
