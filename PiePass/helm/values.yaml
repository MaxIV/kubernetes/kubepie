global:
  image:
    # -- If set, overrides .image.registry, used in KubePie project to define custom registry for all components.
    registry: ""
    # -- If set, overrides .image.tag, used in KubePie project to define custom tag for all components.
    tag: ""

  # -- If set, overrides container [image pull secrets](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).
  imagePullSecrets: []

  # -- If set, overrides extraLabels, used in KubePie project for all components.
  extraLabels: {}
  # -- If set, overrides extraAnnotations to for all resources.
  extraAnnotations: {}
  # -- If set, overrides podAnnotations, used in KubePie project for all components.
  podAnnotations: {}

  # -- If set, overrides Kubernetes [Node Selector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for Pods for all components.
  nodeSelector: {}
  # -- If set, overrides Kubernetes [Tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for Pods for all components.
  tolerations: []
  # -- If set, overrides [Pod Affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity)
  # rules for all components. If set, a field .podAntiAffinityPreset is ignored.
  affinity: {}

  # -- If set, overrides Default affinity rules for all components.
  # Ignored if "affinity" is set.
  # Possible values are: "soft" and "hard" and "" (empty string).
  podAntiAffinityPreset: ""

  # -- If set, overrides topologySpreadConstraints for pods for all components.
  # The `labelSelectors` are added by Helm Chart to each list item.
  topologySpreadConstraints: []

  # -- If enabled, a default Topology Spread Constraints are created which try to distribute
  # pods across topology.kubernetes.io/zone.
  topologySpreadConstraintsPresetEnabled: true

# -- Number of PiePass instances to run.
# Should be set to 1, multiple replicas are not supported yet.
replicaCount: 1

image:
  # -- Container registry URL.
  registry: quay.io
  # -- Container image repository.
  repository: maxiv/piepass
  # -- Kubernetes [Image PullPolicy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy).
  pullPolicy: IfNotPresent
  # -- Overrides the image tag whose default is the chart appVersion.
  # Has higher priority than .global.image.tag.
  tag: ""

# -- Extra labels to add to PiePass resources.
extraLabels: {}
# -- Extra annotations to add to PiePass resources.
extraAnnotations: {}

serviceAccount:
  # -- Specifies whether a service account should be created.
  create: true
  # -- Annotations to add to the service account.
  annotations: {}
  # -- The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template.
  name: ""

# -- Annotations to add to Pods.
podAnnotations: {}

# -- [Pod security](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod) settings.
podSecurityContext:
  {}
  # fsGroup: 2000

# -- [Container security](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container) settings.
# PiePass does not need privileged access and it is expected to work well with any unprivileged user and group IDs.
# @default -- *secure unprivileged defaults*
securityContext:
  capabilities:
    drop:
      - ALL
  readOnlyRootFilesystem: true
  runAsNonRoot: true
  runAsUser: 65534
  allowPrivilegeEscalation: false

service:
  # -- [Service](https://kubernetes.io/docs/concepts/services-networking/service/) type, one of ClusterIP, NodePort, LoadBalancer.
  type: ClusterIP
  # -- TCP port number the Service will receive connections on.
  port: 80

ingress:
  # -- If true, creates an [Ingress resource](https://kubernetes.io/docs/concepts/services-networking/ingress/).
  enabled: true
  # -- Ingress Class to use to handle this Ingress resource.
  className: ""
  # -- Extra annotations to add to the Ingress.
  annotations:
    {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    # -- Ingress hostname.
    - host: chart-example.local
      # @ignore
      # The default configuration should be good enough.
      paths:
        - path: /
          pathType: ImplementationSpecific
  # -- [Ingress TLS configuration](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls).
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

# -- Resources specification
resources:
  {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: "200m"
  #   memory: "256M"
  # requests:
  #   cpu: "100m"
  #   memory: "128M"

autoscaling:
  # -- If true, then Kubernetes [Horizontal Pod Autoscaler (HPA)](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/) is enabled.
  # Should be disabled, multiple replicas are not supported yet.
  enabled: false
  # -- Minimal number of Pods to which HPA can downscale workloads.
  minReplicas: 1
  # -- Maximal number of Pods to which HPA can downscale workloads.
  maxReplicas: 10
  # -- Threshold, if the average CPU usage is higher, HPA can downscale workloads.
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

podDisruptionBudget:
  # -- If true, a Kubernetes [PodDisruptionBudget](https://kubernetes.io/docs/tasks/run-application/configure-pdb/) is deployed.
  enabled: false
  # -- Minimal number of Pods (or percentage) in
  # [Ready state during nodes draining](https://kubernetes.io/docs/tasks/run-application/configure-pdb/#specifying-a-poddisruptionbudget).
  minAvailable: "1"
  # -- Maximal number of Pods (or percentage) in
  # [NotReady state during nodes draining](https://kubernetes.io/docs/tasks/run-application/configure-pdb/#specifying-a-poddisruptionbudget).
  maxUnavailable: ""

# -- Kubernetes [Node Selector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for Pods.
nodeSelector: {}
# -- Kubernetes [Tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for Pods.
tolerations: []
# -- [Pod Affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity)
# rules. If set, a field `.podAntiAffinityPreset` is ignored.
affinity: {}

# -- Default affinity rules.
# Ignored if `.affinity` is set.
# Possible values are: "soft" and "hard" and "" (empty string).
podAntiAffinityPreset: "soft"

# -- [Pod Topology Spread Constraints](https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/)
# rules. If set, a field `.topologySpreadConstraintsPresetEnabled` is ignored.
topologySpreadConstraints: []
# Example with zoning:
#topologySpreadConstraints:
#  - maxSkew: 1
#    topologyKey: topology.kubernetes.io/zone
#    whenUnsatisfiable: DoNotSchedule
#  - maxSkew: 1
#    topologyKey: kubernetes.io/hostname
#    whenUnsatisfiable: DoNotSchedule

# -- If enabled, a default Topology Spread Constraints are created which try to distribute
# pods across topology.kubernetes.io/zone.
topologySpreadConstraintsPresetEnabled: true

authentication:
  # -- JWT token claim to use as username in Apache logs.
  remoteUserClaim: preferred_username

  oidc:
    # -- [OpenID provider metadata URL](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfig).
    # Specify URL, including the `/.well-known/openid-configuration` suffix.
    metadataURL: ""
    # -- OpenID [ClientID](https://www.rfc-editor.org/rfc/rfc6749.html#section-2.3).
    clientID: ""
    # -- OpenID [Client Secret](https://www.rfc-editor.org/rfc/rfc6749.html#section-2.3).
    clientSecret: ""
    # -- Cookies encryption password to save encrypted state in a client browser, mandatory field.
    # Must be set to a random string, at least 32 characters. The change of the string will
    # force all clients to perform a new Round-trip to an OpenID provider.
    cryptoPassphrase: ""

    # -- A Kubernetes Secret which must contain fields:
    # metadataURL, clientID, clientSecret, cryptoPassphrase.
    # and an optional value: proxy.
    #
    # If set, this Secret is used instead of "clientSecret" above,
    # so that one don't have to set the OIDC secret via Helm values.
    secretName: ""

    # -- HTTP Proxy used by PiePass to access the configured OIDC provider to validate tokens.
    proxy: ""

    # -- OIDC redirect URL which is sent to an OpenID provider as a callback URL.
    # If not set, then it will redirect to:
    # `https://{{ first host in .Values.ingress }}/kubepie/piedata/oidc/redirect_url`
    # which should work in most cases.
    redirectURL: ""

# -- [Apache access control directives](https://httpd.apache.org/docs/2.4/howto/access.html).
# This setting is evaluated as a template,
# so one can use variables from helm values.
# Require valid-user should work both for OpenID and OAuth2.
# Shared for both OpenID and OAuth2 authentication providers.
authorization: |
  Require valid-user

# -- [OAuth2 specific Apache authorization directives](https://github.com/OpenIDC/mod_oauth2/wiki#authorization).
# For example: `Require oauth2_claim 'aud:kubepie'`
authorizationOAuth2: ""

# -- [OpenID specific Apache authorization directives](https://github.com/OpenIDC/mod_auth_openidc/wiki/Authorization).
# For example: `Require claim 'aud:kubepie'`
authorizationOpenID: ""

# -- URLs to which the chart will proxy requests.
# URL to PieCut has to be defined for KupePie installation.
proxyBackends: []
# Values are passed through template function so one can use something like:
# proxyBackends:
#   - http://my-chart-which-uses-this-proxy-{{ .Release.Name }}/
# If `proxyBackendLocationMap` is set, then the value of `proxyBackends` is ignored.

# -- URLs to which the chart will proxy requests based on localtion
# Values are passed through template function.
proxyBackendLocationMap: []
# - locationMatch: "/"
#   backends:
#     - http://example-1.com/
#     - http://example-2.com/

# -- Network CIDRs set here are trusted to send to to PiePass.
# X-Forwarded-For header is parsed for these source networks.
# It is advised to use NetworkPolicy to limit Pods which can connect to the service.
loadBalancerTrustedCIDR:
  - 172.16.0.0/12
  - 10.0.0.0/8
  - 192.168.0.0/16

monitoring:
  # -- If true, a PieTrack sidecar is deployed to monitor Apache.
  # Apache logs are shared with this container.
  enabled: false
  # -- Exporter log level.
  metricsLogLevel: INFO

  image:
    # -- Container registry URL.
    registry: quay.io
    # -- Container image repository.
    repository: maxiv/pietrack
    # -- Kubernetes [Image PullPolicy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy).
    pullPolicy: IfNotPresent
    # -- Overrides the image tag whose default is the chart appVersion.
    # Has higher priority than .global.image.tag.
    tag: ""

  # -- Prometheus exporter resources specification
  resources:
    {}
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #   cpu: "200m"
    #   memory: "128M"
    # requests:
    #   cpu: "50m"
    #   memory: "32M"

  serviceMonitor:
    # -- If true, a [ServiceMonitor](https://prometheus-operator.dev/docs/operator/api/#monitoring.coreos.com/v1.ServiceMonitor) resource is deployed.
    enabled: false
    # -- Interval between Prometheus polls.
    interval: 60s
