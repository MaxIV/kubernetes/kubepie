package main

import (
	"bytes"
	"fmt"
	"html/template"
	"log"
	"log/slog"
	"mime"
	"net/http"
	"net/http/cgi"
	"os"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"gopkg.in/yaml.v3"
)

type FileType string

const (
	FileTypeDirectory   FileType = "directory"
	FileTypeHTML        FileType = "html"
	FileTypeText        FileType = "text"
	FileTypePDF         FileType = "pdf"
	FileTypeApplication FileType = "application"
	FileTypeImage       FileType = "image"
	FileTypeArchive     FileType = "archive"
	FileTypeHDF5        FileType = "hdf5"

	FileTypeUnknown FileType = "unknown"
)

var extraExtensions map[string]FileType = map[string]FileType{
	".hdf":  FileTypeHDF5,
	".h4":   FileTypeHDF5,
	".hdf4": FileTypeHDF5,
	".he2":  FileTypeHDF5,
	".h5":   FileTypeHDF5,
	".hdf5": FileTypeHDF5,
	".he5":  FileTypeHDF5,
}

type FileInfo struct {
	Name     string
	ModTime  string
	FileType FileType
	Size     string
}

type PieCutFlags struct {
	PieDataRestartEnabled     bool `json:"pieDataRestartEnabled,omitempty" yaml:"pieDataRestartEnabled"`
	PieDataTerminationEnabled bool `json:"pieDataTerminationEnabled,omitempty" yaml:"pieDataTerminationEnabled"`
}

type WebDAV struct {
	Username        string `json:"username,omitempty" yaml:"username"`
	Password        string `json:"password,omitempty" yaml:"password"`
	CredentialsFile string `json:"credentialsFile,omitempty" yaml:"credentialsFile"`
}

type TemplateData struct {
	Files             []FileInfo
	ParentDirectory   string
	CurrentDirectory  string
	URLPrefix         string
	WebDAV            WebDAV
	PieCutFlags       PieCutFlags
	ExtraTemplateVars map[string]any
}

type Configuration struct {
	DebugLogPath      string         `json:"debugLogPath,omitempty" yaml:"debugLogPath"`
	TemplatePath      string         `json:"templatePath,omitempty" yaml:"templatePath"`
	WebDAV            WebDAV         `json:"webDAV,omitempty" yaml:"webDAV"`
	PieCutFlags       PieCutFlags    `json:"pieCutFlags,omitempty" yaml:"pieCutFlags"`
	ExtraTemplateVars map[string]any `json:"extraTemplateVars,omitempty" yaml:"extraTemplateVars"`
}

var debugLogger *slog.Logger

func formatSize(bSize int64) string {
	const unit = 1024
	scale := []string{"ki", "Mi", "Gi", "Ti", "Pi", "Ei"}

	if bSize < unit {
		return fmt.Sprintf("%d B", bSize)
	}

	div, exp := int64(unit), 0

	for n := bSize / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}

	return fmt.Sprintf("%.1f %sB", float64(bSize)/float64(div), scale[exp])
}

func guessFileType(name string, isDirectory bool) FileType {
	if isDirectory {
		return "directory"
	}

	ext := strings.ToLower(filepath.Ext(name))

	// Handle custom file types.
	fileType, ok := extraExtensions[ext]
	if ok {
		return fileType
	}

	// Otherwise detect Mime type and split it to 2 parts, if possible.
	mimeType := ""
	mimeSubType := ""

	if tmpMimeType := mime.TypeByExtension(ext); tmpMimeType != "" {
		split := strings.Split(tmpMimeType, "/")

		if len(split) == 2 {
			mimeType = split[0]
			mimeSubType = split[1]
		} else {
			mimeType = tmpMimeType
		}
	}

	switch mimeType {
	case "":
		return FileTypeUnknown

	case "text":
		switch mimeSubType {
		case "html":
			return FileTypeHTML

		default:
			return FileTypeText
		}

	case "application":
		switch mimeSubType {
		case "pdf":
			return FileTypePDF

		case "zip":
			return FileTypeArchive
		case "tar":
			return FileTypeArchive

		default:
			return FileTypeApplication
		}

	case "image":
		return FileTypeImage

	default:
		return "file"
	}
}

func canReadFile(path string) bool {
	const (
		// See "man access" for description.
		_R_OK       = 4     // Check for Read permission.
		_AT_EACCESS = 0x200 // Use Effective User access.
	)

	if err := syscall.Faccessat(0, path, _R_OK, _AT_EACCESS); err != nil {
		return false
	}

	return true
}

func getDirectoryListing(dir string) ([]FileInfo, error) {
	entries, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	var files []FileInfo

	for _, entry := range entries {
		fullPath := filepath.Join(dir, entry.Name())

		// Skip files which we cannot read.
		if !canReadFile(fullPath) {
			continue
		}

		fStat, err := os.Stat(fullPath)
		if err != nil {
			continue // Ignore errors.
		}

		fileType := guessFileType(entry.Name(), entry.IsDir())

		files = append(files, FileInfo{
			Name:     entry.Name(),
			ModTime:  fStat.ModTime().Format(time.RFC1123),
			FileType: fileType,
			Size:     formatSize(fStat.Size()),
		})
	}

	return files, nil
}

func getParentDirectory(path string) string {
	parent := filepath.Dir(filepath.Clean(path))
	if parent == path {
		return ""
	}

	if parent == "/" {
		return parent
	}

	return strings.TrimSuffix(parent, "/") + "/"
}

func logDebug(msg string, args ...any) {
	if debugLogger != nil {
		debugLogger.Debug(msg, args...)
	}
}

func logError(msg string, args ...any) {
	if debugLogger != nil {
		debugLogger.Error(msg, args...)
	}
}

func loadConfig() (*Configuration, error) {
	cfgPath := os.Getenv("PIEDIR_CONFIGURATION_PATH")
	if cfgPath == "" {
		cfgPath = "/etc/piedir/piedir.yaml"
	}

	var configuration Configuration

	f, err := os.Open(cfgPath)
	if err != nil {
		return nil, fmt.Errorf("can not open Configuration File: %w", err)
	}
	defer f.Close()

	if err := yaml.NewDecoder(f).Decode(&configuration); err != nil {
		return nil, fmt.Errorf("can not parse configuration: %w", err)
	}

	return &configuration, nil
}

func main() {
	configuration, err := loadConfig()
	if err != nil {
		log.Fatalln(err)
	}

	if configuration.DebugLogPath != "" {
		logFile, err := os.OpenFile(configuration.DebugLogPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.ModePerm)
		if err != nil {
			log.Fatalln(err)
		}

		defer logFile.Close()

		debugLogger = slog.New(slog.NewTextHandler(logFile, &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
		}))
	}

	err = cgi.Serve(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logDebug("Received request")

		// Get the template path from an environment variable.
		templatePath := configuration.TemplatePath
		if templatePath == "" {
			templatePath = "/var/www/html/templates/piedir.html"
		}

		// Trim URL path prefix, if enabled.
		urlPrefix := os.Getenv("PIEDIR_URL_PREFIX")

		var requestFilePath string

		if urlPrefix == "" {
			requestFilePath = r.URL.Path
		} else {
			requestFilePath = strings.TrimPrefix(r.URL.Path, urlPrefix)
		}

		urlPrefix = strings.TrimSuffix(urlPrefix, "/")

		// Current directory based on the request.
		rootDir := os.Getenv("CONTEXT_DOCUMENT_ROOT")
		if rootDir == "" {
			http.Error(w, "CGI misconfiguration, empty CONTEXT_DOCUMENT_ROOT", http.StatusInternalServerError)
			logError("CGI misconfiguration, empty CONTEXT_DOCUMENT_ROOT")

			return
		}

		// Build extra variables from CGI.
		logDebug("CGI configuration",
			"PIEDIR_TEMPLATE_PATH", templatePath,
			"PIEDIR_URL_PREFIX", urlPrefix,
			"CONTEXT_DOCUMENT_ROOT", rootDir,
			"extraTemplateVars", configuration.ExtraTemplateVars,
		)
		logDebug("OS Environment", "envs", os.Environ())

		dir := filepath.Join(rootDir, strings.TrimPrefix(requestFilePath, "/"))

		logDebug("Index directory", "path", dir)

		// List directory and its files, prepare Template Context.
		files, err := getDirectoryListing(dir)
		if err != nil {
			http.Error(w, "Error reading directory", http.StatusInternalServerError)
			logError("Error reading directory", "path", dir, "error", err)

			return
		}

		logDebug("Indexed files", "files", files)

		data := TemplateData{
			Files:             files,
			ParentDirectory:   getParentDirectory(r.URL.Path),
			CurrentDirectory:  strings.TrimPrefix(strings.TrimPrefix(r.URL.Path, urlPrefix), "/"),
			URLPrefix:         urlPrefix,
			PieCutFlags:       configuration.PieCutFlags,
			WebDAV:            configuration.WebDAV,
			ExtraTemplateVars: configuration.ExtraTemplateVars,
		}

		logDebug("Template context", "context", data)

		tmpl, err := template.ParseFiles(templatePath)
		if err != nil {
			http.Error(w, "Error loading template", http.StatusInternalServerError)
			logError("Error loading template", "path", templatePath, "error", err)

			return
		}

		var bb = &bytes.Buffer{}
		err = tmpl.Execute(bb, data)
		if err != nil {
			http.Error(w, "Error rendering template", http.StatusInternalServerError)
			logError("Error rendering template", "error", err)

			return
		}

		logDebug("Rendered template", "text", bb.String())

		w.Header().Set("Content-Type", "text/html")
		if _, err := w.Write(bb.Bytes()); err != nil {
			http.Error(w, fmt.Sprintf("Can not send to client: %s", err), http.StatusInternalServerError)
			logError("Can not send to client", "error", err)

			return
		}

		logDebug("Completed request")
	}))

	if err != nil {
		log.Fatalf("Error running CGI server: %v", err)
	}
}
