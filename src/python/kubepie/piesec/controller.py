import base64
import json
import logging

import jsonpatch
from flask import Flask, current_app, jsonify, request
from kubepie.common.piecrust import *

from .plugins import *

logger = logging.getLogger('KubePie.PieSec.Controller')


def terminal_response(request_uid, permissive=False, error_msg=''):
    """Return AdmissionReview object to allow/forbid request without patching"""
    if permissive:
        logger.info('Requst %s has been allowed without mutating securityContext (permissive mode). %s', 
                    request_uid, error_msg)
        return jsonify({"apiVersion": "admission.k8s.io/v1",
                        "kind": "AdmissionReview",
                        "response": {
                            "uid": request_uid,
                            "allowed": True,
                        }
                        })
    logger.info('Requst %s has been forbidden. %s', request_uid, error_msg)
    response = {
        "apiVersion": "admission.k8s.io/v1",
        "kind": "AdmissionReview",
        "response": {
            "uid": request_uid,
            "allowed": False,
        }
    }
    if error_msg:
        response['response']['status'] = {
            "code": 403,
            "message": error_msg
        }
    return jsonify(response)


#
# Flask app
#
admission_controller = Flask(__name__)

# set common logger format for Flask app
for handler in admission_controller.logger.handlers:
    handler.setFormatter(kubepie_logs_formatter)
# permissive mode: allow Pod to start without mutating securityContext 
# when no UID/GID found
admission_controller.config['PERMISSIVE'] = False
# map plugin to use
admission_controller.config['MAP_PLUGIN'] = 'static'
# map configuration files
admission_controller.config['MAP_CONFIG_FILES'] = []


#
# Mutation webhook implementation
#
@admission_controller.route('/mutate/pods', methods=['POST'])
def deployment_webhook_mutate():
    request_info = request.get_json()
    logger.debug("Request JSON: %s", json.dumps(request_info))
    permissive = current_app.config['PERMISSIVE']
    piesec_patch = []

    # get request ID
    try:
        request_uid = request_info['request']['uid']
    except KeyError as e:
        logger.error(
            'Admission controller received malformed request. Error: no request UID provided.')
        return terminal_response(request_uid)
    
    # get pod Spec
    try:
        pod_spec = request_info["request"]["object"]["spec"]
    except KeyError as e:
        logger.error(
            'Admission controller received malformed request. Error: cannot find object spec.')

    # get claims
    try:
        claims_str = request_info['request']['object']['metadata']['annotations'][PIESEC_CLAIMS_ANNOTATION]
        claims = json.loads(claims_str)
    except KeyError as e:
        logger.error(
            'Pod specification is missing mandatory %s annotation. No ID mapping is possible.',
            PIESEC_CLAIMS_ANNOTATION)
        return terminal_response(
            request_uid,
            permissive,
            'Claims annoation {} is missing'.format(PIESEC_CLAIMS_ANNOTATION))
    except json.JSONDecodeError as e:
        logger.error('Pod %s annotation does not contain valid JSON. %s',
                     PIESEC_CLAIMS_ANNOTATION, str(e))

    # get UID/GID mapping
    try:
        mapclass = PIESEC_PLUGINS[current_app.config['MAP_PLUGIN']]
        mapper = mapclass(
            current_app.config['MAP_CONFIG_FILES'])  # type: UserMap
        if not mapper.map_user(claims):
            logger.error('Failed to map user to UID/GID')
            return terminal_response(request_uid, permissive,
                                     'Failed to find UID/GID mapping for user')
    except PieSecConfigException as e:
        logger.error('%s', str(e))
        return terminal_response(request_uid, permissive,
                                 'PieSec configuration exception.')

    # Pod securityContext enforcement
    podSecurityContext = {}
    op = "add"
    if "securityContext" in pod_spec:
        podSecurityContext = pod_spec["securityContext"]
        op = "replace"
        podSecurityContext.update({
            "runAsNonRoot": True,
            "runAsGroup": mapper.gid(),
            "runAsUser": mapper.uid(),
            "supplementalGroups":  mapper.sgids()
        })

        piesec_patch.append({"op": op,
                             "path": "/spec/securityContext",
                             "value": podSecurityContext})

    # Container securityContext enforcement
    for ctype in ["containers", "initContainers", "ephemeralContainers"]:
        if ctype in pod_spec:
            for cidx, container_spec in enumerate(pod_spec[ctype]):
                securityContext = {}
                op = "add"
                if "securityContext" in container_spec:
                    securityContext = container_spec["securityContext"]
                    op = "replace"
                securityContext.update({
                    "allowPrivilegeEscalation": False,
                    "capabilities": {"drop": ["ALL"]},
                    "privileged": False,
                    "runAsNonRoot": True,
                    "runAsGroup": mapper.gid(),
                    "runAsUser": mapper.uid()
                })
                piesec_patch.append({"op": op,
                                     "path": f"/spec/{ctype}/{cidx}/securityContext",
                                     "value": securityContext})

    # return patch in AdmissionReview
    logger.debug("Patching the pod with: %s", json.dumps(piesec_patch))
    base64_patch = base64.b64encode(jsonpatch.JsonPatch(
        piesec_patch).to_string().encode("utf-8")).decode("utf-8")
    return jsonify({"apiVersion": "admission.k8s.io/v1",
                    "kind": "AdmissionReview",
                    "response": {
                        "uid": request_uid,
                        "allowed": True,
                        "patchType": "JSONPatch",
                        "patch": base64_patch
                    }
                    })


#
# Health check endpoint
#
@admission_controller.route('/healthz', methods=['GET'])
def deployment_healthz():
    return jsonify('OK')
