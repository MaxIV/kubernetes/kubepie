import logging

from kubepie.common.getenv import getenv

#
# Configure logging
#

kubepie_logs_formatter = logging.Formatter(
    "[%(asctime)s] [%(process)d] [%(name)s] [%(levelname)s] [%(process)d] [%(message)s]"
)

class _Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class _Logger(metaclass=_Singleton):
    def __init__(self) -> None:
        # Initialize KubePie logging namespace
        logger = logging.getLogger("KubePie")
        logger.setLevel(level=getenv("LOG_LEVEL", "INFO").upper())
        log_handler_stderr = logging.StreamHandler()
        log_handler_stderr.setFormatter(kubepie_logs_formatter)
        logger.addHandler(log_handler_stderr)

        self._logger = logger

    def get_logger(self):
        return self._logger


logger = _Logger().get_logger()

#
# Constants
#
KUBEPIE_DOMAIN = "kubepie.io"

PIESEC_CLAIMS_ANNOTATION = "piesec.{}/claims".format(KUBEPIE_DOMAIN)
PIESEC_INSTANCE_LABEL = "piesec.{}/instance".format(KUBEPIE_DOMAIN)

PIEDATA_K8S_GROUP = KUBEPIE_DOMAIN
PIEDATA_K8S_VERSION = "v1alpha2"
PIEDATA_K8S_KIND = "PieData"
PIEDATA_K8S_KIND_PLURAL = "piedata"

PIETRACK_DEFAULT_IS_ACTIVE_TIMEOUT = 120
