ARG BASE_IMAGE
FROM ${BASE_IMAGE}
RUN apt-get update && apt-get upgrade -y && apt-get clean -y

ARG KUBEPIE_COMPONENT=piecut

COPY src /app/src
COPY src/web/builtin/. /app/static/builtin/
COPY src/web/piecut/. /app/static/piecut/
COPY src/web/piecut.html /app/static/piecut.html

RUN mkdir -p /app/lib/python/kubepie && \
    mv /app/src/python/kubepie/__init__.py                   /app/lib/python/kubepie/ && \
    mv /app/src/python/kubepie/common                        /app/lib/python/kubepie/ && \
    mv /app/src/python/kubepie/${KUBEPIE_COMPONENT}          /app/lib/python/kubepie/ && \
    mv /app/src/python/${KUBEPIE_COMPONENT}.py               /app/entrypoint.py && \
    rm -rf /app/src && \
    pip install -r /app/lib/python/kubepie/common/requirements.txt \
    -r /app/lib/python/kubepie/${KUBEPIE_COMPONENT}/requirements.txt

ENV PYTHONPATH="$PYTHONPATH:/app/lib/python"

# Set variables are Docker container files structure differs from the source
# code structure
ENV PIECUT_STATIC_FILES_PATH="/app/static/"
ENV PIECUT_FAVICON_PATH="/app/static/builtin/img/favicon.ico"
ENV PIECUT_WAIT_LOOP_FILE="piecut.html"

# Mark variables used to configure PieCut.
ENV PIECUT_BIND_HOST=""
ENV PIECUT_BIND_PORT=""
ENV PIECUT_NAMESPACE=""
ENV PIECUT_INSTANCE_NAME=""
ENV PIECUT_DEBUG_ACCESS_TOKEN=""
ENV PIECUT_EXTERNAL_URL=""
ENV PIECUT_EXTRA_PIEDATA_VALUES_FILE=""


WORKDIR /app/
CMD ["python", "/app/entrypoint.py"]
