import os
from typing import Any, Optional


def getenv(
        var_name: str,
        default_value: Optional[Any] = None) -> Any:
    '''
    Returns environment variable value if set and not empty,
    otherwise returns default_value.
    '''
    res = os.getenv(var_name)

    if res is None:
        return default_value

    if len(res) == 0:
        return default_value

    return res


def getenv_bool(
        var_name: str,
        default_value: bool = False) -> bool:
    '''
    Returns boolean environment variable value.
    Following strings interpreted as True: "1", "true", "True"
    Everything else if False
    '''
    res = os.getenv(var_name)

    if res is None:
        return default_value

    return res.lower() in {"1", "true"}
