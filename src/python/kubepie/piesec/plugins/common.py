import os
import json

class PieSecConfigException(Exception):
    pass

class UserMap(object):
    """ Common abstract class for getting user data"""
    def __init__(self, configs=[], defaults='{}') -> None:
        # load default JSON config
        self.config = json.loads(defaults)
        # update with user-supplied config files
        try:
            for config in configs:
                with open(config, 'r') as c_conf_f:
                    self.config.update(json.load(c_conf_f))
        except FileNotFoundError as e:
            raise PieSecConfigException('Failed to read config file {}. {}'.format(config, str(e)))
        except json.JSONDecodeError as e:
            raise PieSecConfigException('Failed to parse config file {}. {}'.format(config, str(e)))
        
        # define data structure
        self.data = {
            'uid': None,
            'gid': None,
            'sgids': []
        }

    def map_user(self, claims) -> bool:
        raise NotImplementedError()

    def uid(self) -> int:
        return self.data['uid']

    def gid(self) -> int:
        return self.data['gid']
    
    def sgids(self) -> list[int]:
        return self.data['sgids']

