import logging
from dataclasses import dataclass

from aiohttp import web

from kubepie.common import kube_state
from kubepie.common.piecrust import logger

from kubepie.piecut.lib import json_encoder
from kubepie.piecut.web.middleware import compute_redirect_url, redirect_to_deployed
from kubepie.piecut.web.piedata import create_piedata
from kubepie.piecut.web.piedata import kick_piedata as trigger_restart
from kubepie.piecut.web.piedata import kill_piedata as trigger_stop
from kubepie.piecut.web.piedata import redirect_to_wait, report_failure
from kubepie.piecut.web.user_info import user_status_get, user_status_get_all

from kubernetes_asyncio import client
from kubernetes_asyncio.client.api_client import ApiClient


@dataclass
class HandlersConfig:
    include_token_claims_to_piedata: bool
    restart_enabled: bool
    termination_enabled: bool


__HANDLERS_CONFIG_APP_KEY = "handlers_config"


def handlers_config_set(app: web.Application, cfg: HandlersConfig):
    app[__HANDLERS_CONFIG_APP_KEY] = cfg


def handlers_config_get(request: web.Request) -> HandlersConfig:
    return request.app[__HANDLERS_CONFIG_APP_KEY]


async def is_ready(_request: web.Request) -> web.Response:
    """Kubernetes readiness check"""
    async with ApiClient() as api:
        # For some reason mypy type annotations give typing error here
        # while the code should be valid.
        version = await client.VersionApi(api).get_code()  # type: ignore

    return web.Response(text=str(version))


async def is_alive(_request: web.Request) -> web.Response:
    """Kubernetes liveness check"""
    async with ApiClient() as api:
        # For some reason mypy type annotations give typing error here
        # while the code should be valid.
        version = await client.VersionApi(api).get_code()  # type: ignore

    return web.Response(text=str(version))


async def dump_statuses(request: web.Request) -> web.Response:
    """Dumps active PieData releases as JSON"""
    statuses = user_status_get_all(request)

    return web.json_response(statuses, dumps=json_encoder.JSONEncoder().encode)


def init_favicon_handler(path: str):
    """
    Inits a handler which serves a static favicon.ico file.
    """

    async def __favicon(request: web.Request) -> web.FileResponse:
        """Server a static favicon.ico"""
        return web.FileResponse(path=path)

    return __favicon


async def spawn_piedata(request: web.Request) -> web.Response:
    """
    Creates a new PieData or redirects to an existing one,
    if it is already deployed
    """
    user_status, user_info = user_status_get(request)
    config = handlers_config_get(request)

    if user_info is None:
        return await report_failure(logger, request, "No user_info", None, None, None)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Got request to spawn_piedata from %s", user_info)
        logger.debug("user_status for %s is %s", user_info.sub, user_status)

    if user_status is None:
        try:
            await create_piedata(
                request=request,
                user_info=user_info,
                termination_enabled=config.termination_enabled,
                restart_enabled=config.restart_enabled,
                include_claims_json=config.include_token_claims_to_piedata,
            )
            return await redirect_to_wait(request, user_info)
        except client.ApiException as e:
            return await report_failure(
                logger, request, "Can not start a PieData", user_info, user_status, e
            )

    elif user_status.type_ == kube_state.PieDataStateType.ConditionDeployed:
        return await redirect_to_deployed(request, user_info)

    elif user_status.type_ in kube_state.PIEDATA_RELEASE_UNRECOVERABLE_STATES:
        return await report_failure(
            logger,
            request,
            "PieDeck reported helm release error",
            user_info,
            user_status,
            None,
        )

    else:
        return await redirect_to_wait(request, user_info)


async def kick_piedata(request: web.Request) -> web.Response:
    """
    Restarts running PieData Pods.
    """
    user_status, user_info = user_status_get(request)

    if user_info is None:
        return await report_failure(logger, request, "No user_info", None, None, None)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Got request to kick_piedata from %s", user_info)
        logger.debug("user_status for %s is %s", user_info.sub, user_status)

    if user_status is None:
        return await report_failure(
            logger,
            request,
            "Can not restart PieData: PieData is not running",
            user_info,
            user_status,
            None,
            web.HTTPBadRequest,
        )

    try:
        await trigger_restart(request, user_info, user_status)
    except web.HTTPClientError as e:
        return await report_failure(
            logger,
            request,
            "Can not trigger PieData restart",
            user_info,
            user_status,
            e,
            e,
        )
    except client.exceptions.ApiException as ex:
        if ex.status == 403:
            return await report_failure(
                logger,
                request,
                "PieData restart operation is not allowed",
                user_info,
                user_status,
                ex,
                web.HTTPForbidden,
            )
        else:
            return await report_failure(
                logger,
                request,
                "Can not trigger PieData restart",
                user_info,
                user_status,
                ex,
                ex,
            )

    return await redirect_to_wait(request, user_info)


async def kill_piedata(request: web.Request) -> web.Response:
    """
    Stops PieData Pods and deletes the PieData instance.
    """
    user_status, user_info = user_status_get(request)

    if user_info is None:
        return await report_failure(logger, request, "No user_info", None, None, None)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Got request to kick_piedata from %s", user_info)
        logger.debug("user_status for %s is %s", user_info.sub, user_status)

    if user_status is None:
        return await report_failure(
            logger,
            request,
            "Can not restart PieData: PieData is not running",
            user_info,
            user_status,
            None,
            web.HTTPBadRequest,
        )

    try:
        await trigger_stop(request, user_info, user_status)
    except web.HTTPClientError as e:
        return await report_failure(
            logger,
            request,
            "Can not trigger PieData stop",
            user_info,
            user_status,
            e,
            e,
        )
    except client.exceptions.ApiException as ex:
        if ex.status == 403:
            return await report_failure(
                logger,
                request,
                "PieData stop operation is not allowed",
                user_info,
                user_status,
                ex,
                web.HTTPForbidden,
            )
        else:
            return await report_failure(
                logger,
                request,
                "Can not trigger PieData stop",
                user_info,
                user_status,
                ex,
                ex,
            )

    return web.json_response(data={"stop": "success"})


async def get_status(request: web.Request) -> web.Response:
    """
    Returns current user's PieData status as:
        {
            'status': {{ one of kubepie.common.kube_state.PieDataStateType}},
            'is_ready': {{ True | False }},
            'redirect_to': {{ URL where the PieData is available }},
            'user': {
                'preferred_username': {{ preferred_username (fallback to name, than email) from JWT }},
                'given_name': {{ given_name from JWT }},
                'family_name': {{ family_name from JWT }},
                'name': {{ name from JWT }},
                'email': {{ email from JWT }}
            }
        }
    """
    user_status, user_info = user_status_get(request)
    config = handlers_config_get(request)

    if user_info is None:
        return await report_failure(logger, request, "No user_info", None, None, None)

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Got request to get_status from %s", user_info)

    # Make sure user_name is defined
    if user_info.preferred_username != "":
        user_name = user_info.preferred_username
    elif user_info.name != "":
        user_name = user_info.name
    else:
        user_name = user_info.email

    if user_status is None:
        # A new PieData must be created.
        await create_piedata(
            request=request,
            user_info=user_info,
            restart_enabled=config.restart_enabled,
            termination_enabled=config.termination_enabled,
            include_claims_json=config.include_token_claims_to_piedata,
        )

        return web.json_response(
            {
                "status": kube_state.PieDataStateType.ConditionCreated.value,
                "user": {
                    "preferred_username": user_name,
                    "name": user_info.name,
                    "given_name": user_info.given_name,
                    "family_name": user_info.family_name,
                    "email": user_info.email,
                },
                "is_ready": False,
            }
        )

    if user_status.type_ in kube_state.PIEDATA_RELEASE_UNRECOVERABLE_STATES:
        return await report_failure(
            logger=logger,
            request=request,
            message="PieDeck reported helm release error",
            exception=None,
            user_info=user_info,
            user_status=user_status,
        )

    status = {
        "status": user_status.type_.value,
        "user": {
            "preferred_username": user_name,
            "name": user_info.name,
            "given_name": user_info.given_name,
            "family_name": user_info.family_name,
            "email": user_info.email,
        },
        "is_ready": (
            user_status.type_ == kube_state.PieDataStateType.ConditionDeployed
        ),
        "redirect_to": compute_redirect_url(request, user_info),
    }

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug("Status: %s", status)

    return web.json_response(status)
