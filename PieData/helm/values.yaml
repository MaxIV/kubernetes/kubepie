global:
  image:
    # -- If set, overrides .image.registry, used in KubePie project to define custom registry for all components.
    registry: ""
    # -- If set, overrides .image.tag, used in KubePie project to define custom tag for all components.
    tag: ""

  # -- If set, overrides container [image pull secrets](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).
  imagePullSecrets: []

  # -- If set, overrides extraLabels, used in KubePie project for all components.
  extraLabels: {}
  # -- If set, overrides extraAnnotations to for all resources.
  extraAnnotations: {}
  # -- If set, overrides podAnnotations, used in KubePie project for all components.
  podAnnotations: {}

  # -- If set, overrides Kubernetes [Node Selector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for Pods for all components.
  nodeSelector: {}
  # -- If set, overrides Kubernetes [Tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for Pods for all components.
  tolerations: []
  # -- If set, overrides [Pod Affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity)
  # rules for all components. If set, a field .podAntiAffinityPreset is ignored.
  affinity: {}

  # -- If set, overrides Default affinity rules for all components.
  # Ignored if "affinity" is set.
  # Possible values are: "soft" and "hard" and "" (empty string).
  podAntiAffinityPreset: ""

  # -- If set, overrides topologySpreadConstraints for pods for all components.
  # The `labelSelectors` are added by Helm Chart to each list item.
  topologySpreadConstraints: []

# -- Number of PieData instances to run.
# Should be set to 1, multiple replicas are not supported yet.
replicaCount: 1

image:
  # -- Container registry URL.
  registry: quay.io
  # -- Container image repository.
  repository: maxiv/piedata
  # -- Kubernetes [Image PullPolicy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy).
  pullPolicy: IfNotPresent
  # -- Overrides the image tag whose default is the chart appVersion.
  # Has higher priority than .global.image.tag.
  tag: ""

# -- Container [image pull secrets](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).
imagePullSecrets: []
# -- Overrides chart name in the deployed component names.
nameOverride: ""
# -- Overrides full name of components.
fullnameOverride: ""

# -- If set, a PieData Pod will have the hostname defined in this value.
hostnameOverride: ""

serviceAccount:
  # -- Specifies whether a service account should be created.
  create: true
  # -- Annotations to add to the service account.
  annotations: {}
  # -- The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template.
  name: ""

# -- Extra labels to add to PiePass resources.
extraLabels: {}
# -- Extra annotations to add to PiePass resources.
extraAnnotations: {}

# -- Annotations to add to Pods.
podAnnotations: {}

# -- [Pod security](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod) settings.
podSecurityContext:
  {}
  # fsGroup: 2000

# -- [Container security](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container) settings.
# Note! It will be mutated by [PieSec](https://artifacthub.io/packages/helm/piesec/piesec) on Pod start.
securityContext: {}

service:
  # -- [Service](https://kubernetes.io/docs/concepts/services-networking/service/) type, one of ClusterIP, NodePort, LoadBalancer.
  type: ClusterIP

ingress:
  # -- If true, creates an [Ingress resource](https://kubernetes.io/docs/concepts/services-networking/ingress/).
  enabled: false
  # -- Ingress Class to use to handle this Ingress resource.
  className: ""
  # -- Extra annotations to add to the Ingress.
  annotations:
    {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    # -- Ingress hostname.
    - host: chart-example.local
      # @ignore
      # The default configuration should be good enough.
      paths:
        - path: /
          pathType: ImplementationSpecific
  # -- [Ingress TLS configuration](https://kubernetes.io/docs/concepts/services-networking/ingress/#tls).
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

# -- Init container resources specification.
# Init container used to generate stateless WebDAV credentials.
initResources:
  {}
  # TODO: refactor to .init.resources + more init options
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 500m
  #   memory: 512Mi
  # requests:
  #   cpu: 10m
  #   memory: 128Mi

# -- PieData resources specification
resources:
  {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 500m
  #   memory: 512Mi
  # requests:
  #   cpu: 10m
  #   memory: 128Mi

# -- Time for a new PieData Pod to be alive and ready before it is considered to be healthy.
minReadySeconds: 3
# Introduced to workaround NGINX Ingress HTTP/5XX errors during rollouts.

# -- PieData Pod extra initContainers.
extraInitContainers:
  []
  # - name: "init1"
  #   # Templated and appended to .Pod.spec.initContainers.
  #   initContainerSpec: ""

# -- PieData extra services.
# Aimed to run extra container in PieData Pod alongside the main web server for data access.
extraServices:
  {}
  # # Name of the Service.
  # # Name also defines the Ingress path to route requests to this Service.
  # # Do NOT use names like: "pie.*", "kubepie.*" and "webdav.*" as they are reserved.
  # "service-name-1":
  #   # Templated and appended to .Pod.spec.containers.
  #   # Do not set ".ports" in the containerSpec but use ".service" section below.
  #   containerSpec: ""
  #   # Port to which HTTP(S) requests from ingress will be sent.
  #   # If the servicePort is not defined, no HTTP(S) routing will be configured.
  #   service:
  #     port: 1234
  #     protocol: TCP
  #   # Optional, configure ServiceMonitor for container
  #   serviceMonitor:
  #     enabled: false|true
  #     port: 4321
  #     # Optional: custom HTTP path, default - /metrics
  #     path: "/metrics"
  #   # Optional, if true, configure Apache mod_rewrite to rewrite
  #   # proxified HTTP requests to appear them come to the container without
  #   # the PieData's URL prefix (Ingress URL path).
  #   stripIngressPrefix: false
  #   # Optional, if true, configure Apache mod_rewrite to rewrite
  #   # proxified HTTP requests to appear them come to the container without
  #   # the PieData's container name prefix (.name URL path).
  #   stripServiceNamePrefix: false
  #   # If set, preserves original HTTP Host header when performs reverse proxy operations.
  #   proxyPreserveHost: false
  #   # Optional, add HTTP response header to tune client browser security policies, cache etc.
  #   extraHeaders: {}
  #   # Optional, add extra Apache configuration snippet.
  #   # Templated and added to VirtualHost definition generated for this service.
  #   extraApacheConfig: ""
  #   # Optional, disable authentication enforcement for service endpoint,
  #   # making it publicly accessible after "baking".
  #   # Use only if you know what are you doing!
  #   bypassAuthAtMyOwnRisk: false

extraResources:
  []
  # List of Kubernetes resources, i.e. ConfigMaps.
  # Each item must contain a full resource definition
  # and it is passed via Helm template (tpl).
  #
  # - apiVersion: v1
  #   kind: ConfigMap
  #   metadata:
  #     name: {{ include "piedata.fullname" . }}-my-cm
  #     labels:
  #       {{- include "piedata.labels" . | nindent 4 }}
  #     annotations:
  #       {{- include "piedata.annotations" . | nindent 4 }}
  #   data:
  #     file1: content
  #     file2: content

autoscaling:
  # -- If true, then Kubernetes [Horizontal Pod Autoscaler (HPA)](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/) is enabled.
  # Should be disabled, multiple replicas are not supported yet.
  enabled: false
  # -- Minimal number of Pods to which HPA can downscale PieCut workloads.
  minReplicas: 1
  # -- Maximal number of Pods to which HPA can downscale PieCut workloads.
  maxReplicas: 10
  # -- Threshold, if the average CPU usage is higher, HPA can downscale PieCut workloads.
  targetCPUUtilizationPercentage: 80
  # targetMemoryUtilizationPercentage: 80

podDisruptionBudget:
  # -- If true, a Kubernetes [PodDisruptionBudget](https://kubernetes.io/docs/tasks/run-application/configure-pdb/) is deployed.
  enabled: false
  # -- Minimal number of Pods (or percentage) in
  # [Ready state during nodes draining](https://kubernetes.io/docs/tasks/run-application/configure-pdb/#specifying-a-poddisruptionbudget).
  minAvailable: "1"
  # -- Maximal number of Pods (or percentage) in
  # [NotReady state during nodes draining](https://kubernetes.io/docs/tasks/run-application/configure-pdb/#specifying-a-poddisruptionbudget).
  maxUnavailable: ""

# -- Kubernetes [Node Selector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/) for Pods.
nodeSelector: {}
# -- Kubernetes [Tolerations](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) for Pods.
tolerations: []
# -- [Pod Affinity](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity)
# rules. If set, a field `.podAntiAffinityPreset` is ignored.
affinity: {}

# -- Default affinity rules.
# Ignored if `.affinity` field is set.
# Possible values are: "soft", "hard" and "" (empty string).
podAntiAffinityPreset: "soft"

# -- [Pod Topology Spread Constraints](https://kubernetes.io/docs/concepts/scheduling-eviction/topology-spread-constraints/)
# rules. If set, a field `.topologySpreadConstraintsPresetEnabled` is ignored.
topologySpreadConstraints: []
# Example with zoning:
#topologySpreadConstraints:
#  - maxSkew: 1
#    topologyKey: topology.kubernetes.io/zone
#    whenUnsatisfiable: DoNotSchedule
#  - maxSkew: 1
#    topologyKey: kubernetes.io/hostname
#    whenUnsatisfiable: DoNotSchedule

# -- If enabled, a default Topology Spread Constraints are created which try to distribute
# pods across topology.kubernetes.io/zone.
topologySpreadConstraintsPresetEnabled: true

authentication:
  # Apache Basic Authentication to use with WebDAV clients.
  # Only one username-password pair is allowed.
  # If the username and the password are not set and the secretName is not set
  # the PieData Pod will generate a random values on start.
  webDAV:
    # -- If true, WebDAV endpoint is enabled.
    enabled: true
    # -- Apache Basic Authentication username for using with WebDAV.
    # If the username is not set, a random string will be generated by initContainer.
    username: ""
    # -- Apache Basic Authentication password for using with WebDAV.
    # If the password is not set, a random string will be generated by initContainer.
    password: ""
    # -- A Kubernetes Secret which must contain fields [username, password]
    # to use them instead of the .username, .password values.
    # If it is set, then the username, password values above are ignored.
    secretName: ""

  # -- JWT token claim to use as username in Apache logs.
  remoteUserClaim: preferred_username

  oidc:
    # -- [OpenID provider metadata URL](https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderConfig).
    # Must contain the `/.well-known/openid-configuration` suffix.
    metadataURL: ""
    # -- OpenID [ClientID](https://www.rfc-editor.org/rfc/rfc6749.html#section-2.3).
    clientID: ""
    # -- OpenID [Client Secret](https://www.rfc-editor.org/rfc/rfc6749.html#section-2.3).
    clientSecret: ""
    # -- Cookies encryption password to save encrypted state in a client browser, mandatory field.
    # Must be set to a random string, at least 32 characters. The change of the string will
    # force all clients to perform a new Round-trip to an OpenID provider.
    cryptoPassphrase: ""
    # -- A Kubernetes Secret which must contain fields:
    # metadataURL, clientID, clientSecret, cryptoPassphrase.
    #
    # If set, this Secret is used instead of "clientSecret" above,
    # so that one don't have to set the OIDC secret via Helm values.
    secretName: ""
    # -- HTTP Proxy used by Apache to access the configured OIDC provider to validate tokens.
    proxy: ""

    # -- OIDC redirect URL which is sent to an OpenID provider as a callback URL.
    # If not set, then it will redirect to:
    # `https://{{ first host in .Values.ingress }}/kubepie/piedata/oidc/redirect_url`
    # which should work in most cases.
    # In case "extraServices" are deployed, their redirect URLs are formed
    # by adding the extraService' name after the Ingress part.
    redirectURL: ""

authorization:
  allowedUser:
    # -- Claims to allow access only to necessary user(s).
    # Claims can be extended with any other values
    # and handled using logical AND.
    # Claim names must be provided here as they will be in OIDC, OAuth tokens.
    # Claims will be propagated to K8S resources annotations as:
    # `piesec.kubepie.io/{{ claim name }}={{ value }}`
    claims:
      sub: ""
      # email: ""
      # preferred_username: ""
      # aud: ""

  # -- Token audiences allowed to connect to this PieData.
  # If set, then `aud` in `claims` is ignored and the audiences from the
  # `.audiences` field are processed as logical "OR".
  audiences: []

# -- Kubernetes Pod Volumes to mount in PieData pods.
# Provided values are rendered via Helm template.
volumes: []

# -- Kubernetes VolumeMounts.
# To serve the mounted volumes data, the mounts must be subpaths of `/srv/data/`.
# The `/srv` is Apache DocumentRoot, `/srv/data` is an alias to `/` location.
# Provided values are rendered via Helm template.
volumeMounts: []

# -- Extra static assets to be mounted.
# Shortcut to define `volumes` and `volumeMounts` for extra content
# like css or js files.
# Each defined <dir> will be mounted under /srv/static/<dir>/ and
# served by PieData under /static/<dir> URL path prefix.
# Either `content` or `existingSecret` can be defined for each <dir>.
extraAssets:
  []
  # - dir: css
  #   content:
  #     custom-style.css: |
  #       .a { color: red; }
  # - dir: js
  #   existingSecret: "piedir-custom-js"

# -- Source network CIDRs to trust X-Forwarded-For header.
loadBalancerTrustedCIDR:
  - 172.16.0.0/12
  - 10.0.0.0/8
  - 192.168.0.0/16

pieTrack:
  # -- Time from the last request to consider the PieData active.
  isActiveTimeoutSec: 300
  # -- Maximum time which the Piedata is allowed to run.
  # Ignored, if not set.
  maxLifetimeSec: ""

# Configuration options filled by PieCut and reflecting runtime PieCut configuration.
# These will be passed to PieDir for using in template functions.
pieCutFlags:
  # If true, an HTTP request to /kubepie/piecut/kick
  # will restart a running PieData.
  pieDataRestartEnabled: null
  # If true, an HTTP request to /kubepie/piecut/kill
  # will terminate a running PieData.
  pieDataTerminationEnabled: null
  # Defines the PieCut's revision of PieData
  pieDataRevision: "0"

postInstallJob:
  # -- Time in seconds after "helm upgrade" after which the "post-install"
  # hook job fails, so Helm treats the release as failed
  # which will trigger PieEat to delete the Helm release.
  activeDeadlineSeconds: 30

  image:
    registry: quay.io
    # -- Container image repository for the post-install hook job.
    repository: curl/curl
    # -- Container image tag for the post-install hook job.
    tag: 8.9.1

  # -- postInstall job resources specification
  resources:
    {}
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #   cpu: "100m"
    #   memory: "64M"
    # requests:
    #   cpu: "50m"
    #   memory: "32M"

monitoring:
  image:
    # -- PieTrack container registry URL.
    registry: quay.io
    # -- PieTrack container image repository.
    repository: maxiv/pietrack
    # -- Kubernetes [Image PullPolicy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy).
    pullPolicy: IfNotPresent
    # -- Overrides the image tag whose default is the chart appVersion.
    # Has higher priority than .global.image.tag.
    tag: ""

  # -- PieTrack log level.
  # Python log level. Don't set higher than INFO in production.
  logLevel: INFO

  # -- Prometheus exporter resources specification
  resources:
    {}
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #   cpu: "200m"
    #   memory: "128M"
    # requests:
    #   cpu: "50m"
    #   memory: "32M"

  serviceMonitor:
    # -- If true, a [ServiceMonitor](https://prometheus-operator.dev/docs/operator/api/#monitoring.coreos.com/v1.ServiceMonitor) resource is deployed.
    enabled: false
    # -- Interval between Prometheus polls.
    interval: 60s

# If set, the PieData will redirect root URL to the specified service.
# Value must be set to one of the service names from extraServices.
# In most cases PieDir can be disabled when root redirect is in use.
rootRedirectToService: ""

# PieDir configuration for Index page rendering.
pieDir:
  # Enable PieData's PieDir autoindex.
  enabled: true

  # If set, PieDir prints debug log to this file inside PieData container.
  debugLogPath: ""

  # HTML template path used by PieDir to render autoindex.
  templatePath: "/var/www/html/templates/piedir.html"

  # Extra variables to be included in PieDir configuration.
  # These variables will be available in templates under .extraTemplateVars.
  # The format should be any valid YAML.
  # The value is passed through Helm template.
  extraTemplateVars: {}

# Token claims JSON data.
# The values are not used in the chart but might be set here to
# use them in custom templates, for instance, in extraServices.
# The claims can be set by PieCut. See PieCut documentation.
tokenClaimsJSON: ""

# If true, Kubernetes will inject environment variables
# with information about Services in format matching Docker
# service links. Should be disabled unless absolutely necessary.
enableServiceLinks: false

networkPolicy:
  # -- If true, then a default NetworkPolicy resources are created.
  # By default, if the policy is enabled, all access is blocked.
  # One need to allow at least access from Kubernetes Ingress controller
  # and to OpenID Connect provider to allow OpenID Connect auto-discovery.
  enabled: false

  # -- Kubernetes NetworkPolicy .spec.ingress values.
  ingress: []
  # -- Kubernetes NetworkPolicy .spec.egress values.
  egress: []
