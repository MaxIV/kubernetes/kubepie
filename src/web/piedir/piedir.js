document.addEventListener("DOMContentLoaded", () => {
  // pieData URL prefix templated by PieDir
  const urlPrefix = document.getElementById('piedata-url-prefix').content;

  // set webDAV URL
  const webDavURL = document.getElementById('WebDAVURL');
  if (webDavURL) {
    webDavURL.value = `${window.location.origin}${urlPrefix}/webdav/`;
  }

  // check for the service actions in session variables
  if (sessionStorage.getItem("piedata_restart")) {
    sessionStorage.removeItem("piedata_restart");
    restartService();
  }
  if (sessionStorage.getItem("piedata_terminate")) {
    sessionStorage.removeItem("piedata_terminate");
    terminateService();
  }

  // DOM elements to act on
  const btnBack = document.getElementById("btnBack");
  const pieDataRestart = document.getElementById("serviceRestart");
  const pieDataTerminate = document.getElementById("serviceTerminate");

  // send command to PieCut endpoint
  async function sendPieCutRequest(endpoint) {
    const url = new URL(window.location.origin + `/kubepie/piecut/${endpoint}`);

    await fetch(url, { method: "GET" })
      .then(response => {
        if (!response.ok) {
          throw new Error(`HTTP error reaching the PieCut ${endpoint} endpoint at ${url}! Status: ${response.status}`);
        }
      })
      .catch(error => console.error("Error:", error));
  }

  // query revision from PieData service endpoint
  async function getPieDataRevision() {
    try {
      const revisioniUrl = `${window.location.origin}${urlPrefix}/piedata/service/revision`;
      const response = await fetch(revisioniUrl);

      if (!response.ok) {
        throw new Error(`Fetching PieData revision HTTP error: status ${response.status}`);
      }

      const data = await response.text();
      console.log("Current PieData Revision:", data);

      return data;
    } catch (error) {
      console.error("Error fetching PieData Revision:", error);
      return null;
    }
  }

  // loop until the PieData revision changes
  async function waitForRevisionChange(initialRevision) {
    while (true) {
      await new Promise(resolve => setTimeout(resolve, 2000)); // wait in 2 second interval
      const currentRevision = await getPieDataRevision();
      if (currentRevision !== initialRevision) {
        return;
      }
    }
  }

  // PieData restart
  async function restartService() {
    try {
      const currentRevision = await getPieDataRevision();

      await sendPieCutRequest("kick");
      console.log("PieData restart triggered...");

      showWaitModal("Restarting PieData Service...", "Please wait while the service restarts.");

      console.log("Waiting for new PieData revision rollout...");
      await waitForRevisionChange(currentRevision);
      window.location.reload();
    } catch (error) {
      console.error("Error during PieData restart process:", error);
    }
  }

  // PieData termination
  async function terminateService() {
    try {
      await sendPieCutRequest("kill");
      console.log("PieData termination triggered...");

      showWaitModal("Terminating PieData Service...",
        "KubePie is shutting down your service.<br/>You can close the tab now.");
    } catch (error) {
      console.error("Error during PieData termination process:", error);
    }
  }

  // back button in the interface
  btnBack.addEventListener("click", (e) => {
    e.preventDefault();
    window.history.back();
  });

  // reloading the page to make sure the access token in renewed
  // and JS can talk to PieData/PieCut endpoints
  // action is passed via session variables
  if (pieDataRestart) {
    pieDataRestart.addEventListener("click", function () {
      sessionStorage.setItem("piedata_restart", "true");
      window.location.reload();
    })
  };

  if (pieDataTerminate) {
    pieDataTerminate.addEventListener("click", function () {
      sessionStorage.setItem("piedata_terminate", "true");
      window.location.reload();
    })
  };
});

function copyToClipboard(input_id, button) {
  var copyText = document.getElementById(input_id);
  navigator.clipboard.writeText(copyText.value).then(() => {
    let tooltip = new bootstrap.Tooltip(button, {
      title: "Copied!",
      trigger: "manual"
    });
    tooltip.show();

    setTimeout((t) => {
      if (t) {
        t.dispose();
      }
    }, 1500, tooltip);
  });
}

function showWaitModal(message, description) {
  const wait_modal = document.createElement('div');
  wait_modal.innerHTML = `
      <div style="
          position: fixed; top: 0; left: 0; width: 100%; height: 100%;
          background: rgba(0, 0, 0, 0.5); display: flex; justify-content: center; align-items: center;
      ">
          <div style="background: white; padding: 50px; border-radius: 8px;">
              <h3>${message}</h3>
              <p>${description}</p>
          </div>
      </div>
  `;
  document.body.appendChild(wait_modal);
}
