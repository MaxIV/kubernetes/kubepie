## PieCut

PieCut component receives all requests to the KubePie deployment URL
by default, if they are not handled by users' PieData deployments.

PieCut creates a new PieData CRD for each user who does not have an active PieData.
The user's URL is formed as `{{ KubePie base URL }}/{{ the user token's "sub" }}/`
to which the user is later redirected.

See [piedata.py](piecut/src/lib/piedata.py) for more information.

PieCut user request handling flow:

```mermaid
sequenceDiagram
    participant client as HTTP Client
    participant piecut as PieCut
    participant piedata as PieData
    participant kubernetes as Kubernetes

    rect green
        client ->> piecut: HTTP GET /...
    end

    piecut ->> kubernetes: Is PieData running?

    alt Web server is running - internal controller's knowledge
        rect green
            kubernetes ->> piecut: Web server is running
            piecut ->> client: Redirect to PieData
        end
    else Web server is not running
        rect yellow
            kubernetes ->> piecut: Web server is not running
            piecut ->> kubernetes: Request a new PieData
            kubernetes ->> piecut: Will be done one day...
            piecut ->> client: Come back later (HTTP/302)
        end

        loop Until PieData ready
            rect yellow
                client ->> piecut: Is PieData ready?
                piecut ->> kubernetes: Is PieData ready?
            end

            alt Web server is not ready
                rect yellow
                    kubernetes ->> piecut: PieData is not ready yet
                    piecut ->> client: Come back later (HTTP/302)
                end
            else Web server is ready
                rect green
                    kubernetes ->> piecut: PieData is ready
                    piecut ->> client: Redirect to PieData
                end
            end
        end
    end

    rect green
        client ->> piedata: HTTP GET /...
    end
        
    alt Access Token - Valid
        rect green
            piedata ->> client: Here is your data
        end
    else Access Token - Invalid
        rect red
            piedata ->> client: HTTP Unauthorized
        end
    end
```
