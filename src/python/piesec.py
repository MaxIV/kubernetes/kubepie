import logging

from kubepie.common.getenv import getenv
from kubepie.common.piecrust import (PIESEC_CLAIMS_ANNOTATION,
                                     PIESEC_INSTANCE_LABEL)
from kubepie.piesec.wsgi import run

logger = logging.getLogger('KubePie.PieSec')

logger.info("Starting PieSec admission controller.")
logger.info("Configured to mutate Pods with \"%s: %s\" label.",
            PIESEC_INSTANCE_LABEL, getenv('INSTANCE'))
logger.info("Expecting claims JSON in %s Pod annotation",
            PIESEC_CLAIMS_ANNOTATION)

run()
