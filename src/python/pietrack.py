import logging

from kubepie.common.piecrust import *
from kubepie.pietrack.aio import run_blocking

logger = logging.getLogger('KubePie.PieTrack')

logger.info("Starting PieTrack metrics collector for PieData.")

run_blocking()
