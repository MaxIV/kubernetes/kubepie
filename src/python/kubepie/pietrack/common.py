from dataclasses import dataclass
from datetime import datetime


@dataclass
class ClientRequest:
    virtual_host: str
    virtual_host_port: int
    client_hostname: str
    client_username: str | None
    request_timestamp: datetime
    request_method: str
    request_path: str
    request_protocol: str
    response_status: int
    sent_bytes: int
    referer: str | None
    user_agent: str


@dataclass
class ActivityStatus:
    is_active: bool
    TTL_left: int
