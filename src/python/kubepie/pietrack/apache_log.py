import logging
import re
import typing
from datetime import datetime
from time import sleep

from .common import ClientRequest


class ApacheLogParser:
    # The regex cuts client_username's random suffix to represent better metrics.
    apache_vhost_re = re.compile(
        r"^(?P<virtual_host_hostname>\S+)\:(?P<virtual_host_port>\d+)\s+"
        r"(?P<client_hostname>\S+)\s+\-\s+(?P<client_username>\S+)\s+"
        r"\[(?P<request_time>.+)\]\s+"
        r"\"(?P<request_method>\S+)\s+(?P<request_path>\S+)\s+"
        r"(?P<request_protocol>\S+)\"\s+(?P<response_status>\d+)\s+"
        r"(?P<sent_bytes>\d+)\s+\"(?P<referer>\S+)\"\s+\"(?P<user_agent>.+)\""
    )
    apache_datetime_format = "%d/%b/%Y:%H:%M:%S %z"

    def __init__(
        self,
        log_stream: typing.TextIO,
        include_vhosts: typing.Collection[str] | None = None,
        exclude_vhosts: typing.Collection[str] | None = None,
    ) -> None:
        self._include_vhosts = include_vhosts
        self._exclude_vhosts = exclude_vhosts
        self._stream = log_stream
        self.__running = False
        self.logger = logging.getLogger("KubePie.PieTrack.Apache.LogParser")

    def stop(self):
        self.__running = False

    def __iter__(self):
        return self

    def __next__(self) -> ClientRequest:
        self.__running = True

        while self.__running:
            line = self._stream.readline()

            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug("Apache access log line: %s", line)

            if line is None or line == "":
                sleep(1)  # 1 second sleep should be acceptable in our case.
                continue

            re_match = self.apache_vhost_re.match(line)

            if re_match is None:
                continue

            # Filter based on virtual host.
            virtual_host = re_match["virtual_host_hostname"]
            if self._include_vhosts is not None:
                if virtual_host not in self._include_vhosts:
                    continue
            if self._exclude_vhosts is not None:
                if virtual_host in self._exclude_vhosts:
                    continue

            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug('Got activity: "%s"', line)

            sent_bytes = int(re_match["sent_bytes"])
            response_status = int(re_match["response_status"])
            virtual_host_port = int(re_match["virtual_host_port"])
            client_username = re_match["client_username"]
            if client_username == "-":
                client_username = None
            else:
                # We know that the username might be name-{{ random suffix }}
                # so we cut the suffix, if necessary.
                try:
                    end_pos = client_username.rindex("-")
                    client_username = client_username[0:end_pos]
                except ValueError:
                    pass

            referer = re_match["referer"]
            if referer == "-":
                referer = None

            request_date = datetime.strptime(
                re_match["request_time"], self.apache_datetime_format
            )

            cl_request = ClientRequest(
                virtual_host=virtual_host,
                client_hostname=re_match["client_hostname"],
                client_username=client_username,
                referer=referer,
                request_method=re_match["request_method"],
                request_path=re_match["request_path"],
                request_protocol=re_match["request_protocol"],
                request_timestamp=request_date,
                response_status=response_status,
                sent_bytes=sent_bytes,
                user_agent=re_match["user_agent"],
                virtual_host_port=virtual_host_port,
            )

            if self.logger.isEnabledFor(logging.DEBUG):
                self.logger.debug("Parsed ClientRequest: %s", cl_request)

            return cl_request

        raise StopIteration()
