from typing import Dict, Optional, Tuple

from aiohttp import web
from kubepie.common import kube_state
from kubepie.piecut.lib.oidc_user_info import OIDCUserInfo

# Used to load store statuses of PieData_s to aiohttp Application as key.
CONST_PIEDATA_STATUS_KEY = 'piedata_status'


def user_status_init(app: web.Application):
    app[CONST_PIEDATA_STATUS_KEY] = dict()


def user_status_get_all(
        request: web.Request) -> Dict[str, Optional[kube_state.PieDataState]]:
    return request.app[CONST_PIEDATA_STATUS_KEY]


def user_status_get_all_app(
        app: web.Application) -> Dict[str, Optional[kube_state.PieDataState]]:
    return app[CONST_PIEDATA_STATUS_KEY]


def user_status_get(request: web.Request) -> Tuple[
        Optional[kube_state.PieDataState], Optional[OIDCUserInfo]]:
    user_info = user_info_get(request)

    if user_info is None:
        return None, None

    try:
        return request.app[CONST_PIEDATA_STATUS_KEY][user_info.sub], user_info
    except KeyError:
        return None, user_info


def user_status_set(
        request: web.Request,
        user_info: OIDCUserInfo,
        user_state: kube_state.PieDataState):
    request.app[CONST_PIEDATA_STATUS_KEY][user_info.sub] = user_state


def user_status_delete(request: web.Request, user_info: OIDCUserInfo,):
    try:
        del request.app[CONST_PIEDATA_STATUS_KEY][user_info.sub]
    except KeyError:
        pass


def user_info_get(request: web.Request) -> Optional[OIDCUserInfo]:
    try:
        return request['user_info']
    except KeyError:
        return None


def user_info_set(request: web.Request, info: OIDCUserInfo):
    request['user_info'] = info
