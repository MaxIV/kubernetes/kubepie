#!/bin/bash

script_dir="$( dirname -- "$0" )"
img_base="${script_dir}/../../docs/icons"
tmp_dir="$( mktemp -d )"
trap 'rm -rf -- "${tmp_dir}"' EXIT

# favicon.ico
inkscape -w 16 -h 16 -o ${tmp_dir}/16.png ${img_base}/svg/favicon.svg
inkscape -w 32 -h 32 -o ${tmp_dir}/32.png ${img_base}/svg/favicon.svg
inkscape -w 48 -h 48 -o ${tmp_dir}/48.png ${img_base}/svg/favicon.svg
magick convert ${tmp_dir}/16.png ${tmp_dir}/32.png ${tmp_dir}/48.png ${script_dir}/builtin/img/favicon.ico

# logo.png
inkscape -w 128 -o ${script_dir}/builtin/img/logo.png ${img_base}/svg/heptagons/kubepie.svg
